/**
 * Entry point for the GeoGuessr website content script.
 * This script is executed prior to anything else being loaded on the webpage.
 * The website is a SPA, which means the content script could be run as little
 * as one time per tab.
 */

import ContentStateBroker from 'content/endpoint_transition_handler'
import axios from 'axios'
import Message, { ChromeMessageBroker, MessageType, Uniform, UniformType } from 'common/messages'
import AbyssLogger from 'common/abyss_logger'
import { injectScript } from 'content/content_script'
import StorageKeys from 'common/storage'

const messageBroker = new ChromeMessageBroker()
const baseLogger = new AbyssLogger(messageBroker)
const broker = new ContentStateBroker(axios.create({ baseURL: 'https://www.geoguessr.com' }), axios.create({baseURL: 'https://game-server.geoguessr.com'}), messageBroker, baseLogger)

injectScript('canvasInject.js', true)
injectScript('websocketInject.js', true)

let pixelateMap = false
let grayscaleMap = false
let pixelateScale = 75.0
const updateMaterial = () => {
    const defines = []
    const uniforms: Uniform[] = []
    if(pixelateMap) {
        defines.push('PIXELATED')
        uniforms.push({name: 'scaling', type: UniformType.FLOAT, value: [pixelateScale]})
    }
    if(grayscaleMap) {
        defines.push('GRAYSCALE')
    }
    const msg: Message = {
        type: MessageType.UPDATE_MATERIAL,
        shaderMessage: {
            defines: defines,
            uniforms: uniforms
        }
    }
    window.postMessage(msg, '*')
}

let firstFrame = true

messageBroker.registerListener({
    onMessage: (message: Message): void => {
        switch (message.type) {
            case MessageType.FINISHED_FRAME: {
                if (firstFrame) {
                    firstFrame = false
                    chrome.storage.local.get([StorageKeys.PIXELATE_MAP, StorageKeys.PIXELATE_SCALE, StorageKeys.GRAYSCALE], (items) => {
                        pixelateMap = items[StorageKeys.PIXELATE_MAP] == 'true' ? true: false
                        pixelateScale = parseFloat(items[StorageKeys.PIXELATE_SCALE] ?? '75.0')
                        grayscaleMap = items[StorageKeys.GRAYSCALE] == 'true' ? true: false
                        updateMaterial()
                    })
                }
                broker.onStateChanged(window.location.pathname)
                break
            }
            case MessageType.PIXELATE_ON: {
                pixelateMap = true
                updateMaterial()
                break
            }
            case MessageType.PIXELATE_OFF: {
                pixelateMap = false
                updateMaterial()
                break
            }
            case MessageType.PIXELATE_SCALE: {
                pixelateScale = message.scale
                updateMaterial()
                break
            }
            case MessageType.GRAYSCALE_ON: {
                grayscaleMap = true
                updateMaterial()
                break
            }
            case MessageType.GRAYSCALE_OFF: {
                grayscaleMap = false
                updateMaterial()
                break
            }
        }
    },
    messageListenerName: (): string => {
        return 'ContentMain'
    }
})


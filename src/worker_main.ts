/**
 * Entry point for worker script.
 */

import { AbyssTag } from 'common/logging'
import Message, { MessageType } from 'common/messages'
import StorageKeys from 'common/storage'
import { authFunc } from 'twitch/twitch_bot'

chrome.webNavigation.onCompleted.addListener((details) => {
    const msg: Message = {
        type: MessageType.FINISHED_FRAME
    }
    chrome.tabs.sendMessage(details.tabId, msg)
})

const broadcastMessage = (message: Message) => {
    // Forward message to all other tabs
    chrome.tabs.query({}, (tabs) => {
        tabs.forEach(tab => {
            chrome.tabs.sendMessage(tab.id, message)
        })
    })
}

const handleLog = (tag: AbyssTag, message: string): void => {
    chrome.storage.local.get([StorageKeys.LOGS, StorageKeys.DEBUGGABLE], (items) => {
        const debuggable = (items[StorageKeys.DEBUGGABLE] ?? 'false') == 'true'
        if (!debuggable) {
            return
        }
        const logging_data = items[StorageKeys.LOGS] ?? []
        logging_data.push({
            timestamp: Date.now(),
            source: tag,
            message: message
        })
        const newLogs = {}
        newLogs[StorageKeys.LOGS] = logging_data
        chrome.storage.local.set(newLogs)
    })
}

const handleEvent = (message: Message): void => {
    chrome.storage.local.get([StorageKeys.EVENTS, StorageKeys.DEBUGGABLE], (items) => {
        const debuggable = (items[StorageKeys.DEBUGGABLE] ?? 'false') == 'true'
        if (!debuggable) {
            return
        }
        const events_data = items[StorageKeys.EVENTS] ?? []
        events_data.push({
            timestamp: Date.now(),
            event: message
        })
        const new_events = {}
        new_events[StorageKeys.EVENTS] = events_data
        chrome.storage.local.set(new_events)
    })
}

chrome.runtime.onMessage.addListener((message: Message) => {
    if (message.type != MessageType.LOG_INTO_ABYSS) {
        broadcastMessage(message)
        handleEvent(message)
    } else {
        handleLog(message.abyssTag, message.abyssMessage)
    }
    switch(message.type) {
        case MessageType.CONNECT_TWITCH: {
            authFunc(
                false,
                (token) => {
                    chrome.windows.create({ type: 'popup', url: chrome.runtime.getURL(`twitch.html?token=${token}`), width: 300, height: 500 }, (w) => {
                        const obj = {}
                        obj[StorageKeys.CG_WINDOW_OPEN] = 'true'
                        obj[StorageKeys.TWITCH_WINDOW_ID] = w.id
                        chrome.storage.local.set(obj)
                    })
                    const msg: Message = {
                        type: MessageType.TWITCH_CONNECTION_SUCCESS
                    }
                    chrome.runtime.sendMessage(msg)
                },
                () => {
                    const msg: Message = {
                        type: MessageType.TWITCH_CONNECTION_FAIL
                    }
                    chrome.runtime.sendMessage(msg)
                })
            break
        }
        case MessageType.CHANNEL_CONNECTION_SUCCESS: {
            const obj = {}
            obj[StorageKeys.CHANNEL_NAME] = message.channelName
            chrome.storage.local.set(obj)
            break
        }
        case MessageType.CG_READY: {
            const obj = {}
            obj[StorageKeys.CG_READY] = 'true'
            chrome.storage.local.set(obj)
            break
        }
        case MessageType.CG_START_GAME: {
            const obj = {}
            obj[StorageKeys.CG_GAME_ID] = message.game.token
            chrome.storage.local.set(obj)
            break
        }
        case MessageType.CG_GAME_FINISHED: {
            chrome.storage.local.remove(StorageKeys.CG_GAME_ID)
            break
        }
        case MessageType.BLUR_BR_AVATARS: {
            const obj = {}
            obj[StorageKeys.BLUR_BR_AVATARS] = 'true'
            chrome.storage.local.set(obj)
            break
        }
        case MessageType.UNBLUR_BR_AVATARS: {
            const obj = {}
            obj[StorageKeys.BLUR_BR_AVATARS] = 'false'
            chrome.storage.local.set(obj)
            break
        }
        case MessageType.BLUR_BR_GUESSES: {
            const obj = {}
            obj[StorageKeys.BLUR_BR_GUESSES] = 'true'
            chrome.storage.local.set(obj)
            break
        }
        case MessageType.UNBLUR_BR_GUESSES: {
            const obj = {}
            obj[StorageKeys.BLUR_BR_GUESSES] = 'false'
            chrome.storage.local.set(obj)
            break
        }
        case MessageType.HIDE_BR_NAMES: {
            const obj = {}
            obj[StorageKeys.HIDE_BR_NAMES] = 'true'
            chrome.storage.local.set(obj)
            break
        }
        case MessageType.UNHIDE_BR_NAMES: {
            const obj = {}
            obj[StorageKeys.HIDE_BR_NAMES] = 'false'
            chrome.storage.local.set(obj)
            break
        }
        case MessageType.DEBUG_ON: {
            const obj = {}
            obj[StorageKeys.DEBUGGABLE] = 'true'
            chrome.storage.local.set(obj)
            break
        }
        case MessageType.DEBUG_OFF: {
            const obj = {}
            obj[StorageKeys.DEBUGGABLE] = 'false'
            chrome.storage.local.set(obj)
            chrome.storage.local.remove([StorageKeys.EVENTS, StorageKeys.LOGS])
            break
        }
        case MessageType.PIXELATE_ON: {
            const obj = {}
            obj[StorageKeys.PIXELATE_MAP] = 'true'
            chrome.storage.local.set(obj)
            break
        }
        case MessageType.PIXELATE_OFF: {
            const obj = {}
            obj[StorageKeys.PIXELATE_MAP] = 'false'
            chrome.storage.local.set(obj)
            break
        }
        case MessageType.PIXELATE_SCALE: {
            const obj = {}
            obj[StorageKeys.PIXELATE_SCALE] = message.scale || "75.0"
            chrome.storage.local.set(obj)
            break
        }
        case MessageType.GRAYSCALE_ON: {
            const obj = {}
            obj[StorageKeys.GRAYSCALE] = 'true'
            chrome.storage.local.set(obj)
            break
        }
        case MessageType.GRAYSCALE_OFF: {
            const obj = {}
            obj[StorageKeys.GRAYSCALE] = 'false'
            chrome.storage.local.set(obj)
            break
        }
        case MessageType.UPDATE_BLOCKLIST: {
            const obj = {}
            obj[StorageKeys.BLOCK_LIST] = message.blockList ?? []
            chrome.storage.local.set(obj)
            break
        }
    }


})
const clearCGOpen = () => {
    chrome.storage.local.remove([StorageKeys.CG_READY, StorageKeys.CG_WINDOW_OPEN])
}
chrome.tabs.onRemoved.addListener((w,info) => {
    chrome.storage.local.get(StorageKeys.TWITCH_WINDOW_ID, (items) => {
        const twi = parseInt(items[StorageKeys.TWITCH_WINDOW_ID] ?? '-1')
        if (twi == info.windowId) {
            broadcastMessage({
                type: MessageType.TWITCH_WINDOW_CLOSED
            })
            broadcastMessage({
                type: MessageType.CG_DISABLED
            })
            const obj = {}
            obj[StorageKeys.CG_READY] = 'false'
            obj[StorageKeys.CG_WINDOW_OPEN] = 'false'
            chrome.storage.local.set(obj)
        }
    })
})
chrome.runtime.onInstalled.addListener(clearCGOpen)
chrome.runtime.onStartup.addListener(clearCGOpen)
chrome.runtime.onStartup.addListener(()=> {
    handleLog(AbyssTag.WORKER, 'onStartup')
})
chrome.runtime.onSuspend.addListener(()=> {
    handleLog(AbyssTag.WORKER, 'onSuspend')
})
chrome.runtime.onSuspendCanceled.addListener(()=> {
    handleLog(AbyssTag.WORKER, 'onSuspendCanceled')
})

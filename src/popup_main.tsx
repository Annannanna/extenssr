import { ChromeMessageBroker } from 'common/messages'
import React from 'react'
import ReactDOM from 'react-dom'
import Popup from './popup/popup'

const messageBroker = new ChromeMessageBroker()
ReactDOM.render(<Popup messageBroker={messageBroker}/>, document.getElementById('popup'))
(function () {
    class MyWebsocket extends WebSocket {
        constructor(url, protocol) {
            super(url, protocol)
            this.addEventListener('message', (evt) => {
                const data = JSON.parse(evt.data)
                if (data.code === 'NewRound' || data.code === 'GameFinished' || data.code === 'GameStarted') {
                    const battleRoyaleGameState = data.battleRoyaleGameState
                    document.dispatchEvent(new CustomEvent("battle-royale-data", { detail: { rounds: battleRoyaleGameState.rounds, gameId: battleRoyaleGameState.gameId, status: data.battleRoyaleGameState.status } }))
                }
            })
        }
    }
    window.WebSocket = MyWebsocket
})()
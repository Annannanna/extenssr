!(function () {
    // weird globals
    const markers = []
    const lines = []
    let globalMap = null
    const google = window.google
    const targetIcon = {
        path: `M13.04,41.77c-0.11-1.29-0.35-3.2-0.99-5.42c-0.91-3.17-4.74-9.54-5.49-10.79c-3.64-6.1-5.46-9.21-5.45-12.07
            c0.03-4.57,2.77-7.72,3.21-8.22c0.52-0.58,4.12-4.47,9.8-4.17c4.73,0.24,7.67,3.23,8.45,4.07c0.47,0.51,3.22,3.61,3.31,8.11
            c0.06,3.01-1.89,6.26-5.78,12.77c-0.18,0.3-4.15,6.95-5.1,10.26c-0.64,2.24-0.89,4.17-1,5.48C13.68,41.78,13.36,41.78,13.04,41.77z
            `,
        fillColor: "#de3e3e",
        fillOpacity: 0.7,
        scale: 1.2,
        strokeColor: "#000000",
        strokeWeight: 1,
        anchor: new google.maps.Point(14, 43),
        labelOrigin: new google.maps.Point(13.5, 15),
    }
    const guessIconBase = Object.assign({}, targetIcon, {scale: 1})

    // for grabbing the map instance
    !(function () {
        const oldMap = google.maps.Map
        google.maps.Map = Object.assign(
            function (...args) {
                const res = oldMap.apply(this, args)
                this.addListener("idle", () => {
                    if (globalMap != null || this == null) return
                    globalMap = this
                })
                return res
            },
            {
                prototype: Object.create(oldMap.prototype),
            }
        )
    })()

    // generate a random colour
    const randomColour = () => {
        let color = '#'
        for (let index = 0; index < 6; ++index) {
            if (index == 0) {
                return
            }
            color += '0123456789abcdef'[Math.floor(Math.random() * 16)]
        }
        return color
    }

    const addTargetMarker = (target) => {
        if (!target) {
            return
        }
        const targetMarker = new google.maps.Marker({
            position: target,
            url: `http://maps.google.com/maps?q=&layer=c&cbll=${target.lat},${target.lng}`,
            icon: targetIcon,
            map: globalMap,
        })
        google.maps.event.addListener(targetMarker, "click", () => {
            window.open(targetMarker.url, "_blank")
        })
        markers.push(targetMarker)
    }

    const addGuess = (guess, rank, target) => {
        const infowindow = new google.maps.InfoWindow()
        const colour = randomColour()
            const guessIcon = Object.assign({}, guessIconBase, {fillColor: colour})
            const guessMarker = new google.maps.Marker({
                position: guess.position,
                icon: guessIcon,
                map: globalMap,
                label: { color: "#000", fontWeight: "bold", fontSize: "16px", text: `${rank}` },
            })
            markers.push(guessMarker)
            google.maps.event.addListener(guessMarker, "mouseover", () => {
                infowindow.setContent(`
                    <div>
                        <span style="font-size:14px">${guess.username}</span><br>
                        ${guess.distance >= 1 ? parseFloat(guess.distance.toFixed(1)) + "km" : parseInt(guess.distance * 1000) + "m"}<br>
                        ${guess.score}
                    </div>
                `)
                infowindow.open(globalMap, guessMarker)
            })
            google.maps.event.addListener(guessMarker, "mouseout", () => {
                infowindow.close()
            })
            lines.push(
                new google.maps.Polyline({
                    strokeColor: colour,
                    strokeWeight: 4,
                    strokeOpacity: 0.6,
                    geodesic: true,
                    map: globalMap,
                    path: [guess.position, target],
                })
            )
    }

    // TODO: Maybe there's still hope in using TS instead? ev.data corresponds to a Message event
    window.addEventListener('message', (ev) => {
        if (ev.type != 'clear-all' && ev.type != 'show-map-points') {
            return
        }
        document.querySelectorAll('#oldmap-deleters').forEach(element => element.remove())
        while (markers.length > 0) {
            markers.pop()?.setMap(null)
        }
        while (lines.length > 0) {
            lines.pop()?.setMap(null)
        }
        if (ev.type == 'clear-all') {
            return
        }
        const target = ev.data.target
        if (!target) {
            return
        }
        const guesses = ev.data.roundGuesses || []
        // Hide old markers
        const styleNode = document.createElement('style')
        styleNode.innerHTML = `.map-pin { display: none} .result-map__line {display:none;}`
        styleNode.id = 'oldmap-deleters'
        document.head.appendChild(styleNode)
        
        // Add marker for target
        addTargetMarker(target)  

        // Add markers for each guess
        guesses.forEach((guess, index) => addGuess(guess, index + 1, target))
    })
})()
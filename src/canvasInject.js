!(function () {
    const specialVertexShader=`
varying vec3 a;
uniform vec4 b;
attribute vec3 c;
attribute vec2 d;
uniform mat4 e;
void main() {
    vec4 g=vec4(c,1);
    gl_Position=e*g;
    a=vec3(d.xy*b.xy+b.zw,1);
    a*=length(c);
}
    `
    const specialFragmentShader=`
precision highp float;
const float h=3.1415926;
varying vec3 a;
uniform vec4 b;
uniform float f;
uniform sampler2D g;
#ifdef PIXELATED
uniform float scaling;
#endif
void main() {
vec4 color = vec4(1.0, 0.0, 0.0, 1.0);
#ifdef PIXELATED
vec2 texCoord = a.xy / a.z;
texCoord = vec2(ivec2(texCoord * scaling)) / scaling;
texCoord.x = float(int(texCoord.x * scaling)) / scaling;
texCoord.y = float(int(texCoord.y * scaling)) / scaling;
color = vec4(texture2D(g,texCoord).rgb,f);
#else
color = vec4(texture2DProj(g,a).rgb,f);
#endif
#ifdef GRAYSCALE
float gray = (color.r + color.g + color.b) / 3.0;
color.rgb = vec3(gray, gray, gray);
#endif
gl_FragColor = color;
}
`
    let beforeDefines = []
    let beforeUniforms = []
    const beforeCanvasListener = (ev) => { 
        if (ev.data.type !== 'update-material') {
            return
        }
        beforeDefines = ev.data.shaderMessage.defines || []
        beforeUniforms = ev.data.shaderMessage.uniforms || []
    }
    window.addEventListener('message', beforeCanvasListener)
    const aliasCall = (target, methodName, newWrapper) => {
        const oldMethod = target[methodName]
        const newMethod = newWrapper(oldMethod)
        target[methodName] = function() {
            try {
                const ret = newMethod.apply(this, arguments)
                if (target.getError) {
                    target.getError() // clear errors
                }
                return ret
            }catch(e) {
                return null
            }
        }
    }
    const aliasConfig = (target, config) => {
        for (const methodName in config) {
            const wrapper = config[methodName]
            if (typeof wrapper === 'function') {
                aliasCall(target, methodName, wrapper)
            }

        }
    }
    aliasConfig(document, {
        createElement: (oldCreateElement) => function () {
            const element = oldCreateElement.apply(this, arguments)
            const name = arguments[0]

            if (name && name.toLowerCase() === 'canvas') {
                aliasConfig(element, {
                    getContext: (oldGetContext) => function () {
                        const context = oldGetContext.apply(this, arguments)
                        const contextType = arguments[0]
                        if (contextType && contextType.startsWith('webgl')) {
                            let overrideShaderName = 'default'
                            let dirtyUniforms = false
                            const overrideMaterials = {}
                            context.oldShaderSource = context.shaderSource
                            context.oldGetUniformLocation = context.getUniformLocation
                            context.oldAttachShader = context.attachShader
                            context.oldUniform1fv = context.uniform1fv
                            context.oldUniform2fv = context.uniform2fv
                            context.oldUniform3fv = context.uniform3fv
                            const previousUniforms = {}
                            const uniformLocations = {}
                            let customUniforms = []
                            let activeProgram = null
                            let actualProgram = null
                            const draw = () => {
                                window.requestAnimationFrame(() => {
                                    dirtyUniforms = true
                                    element.dispatchEvent(new MouseEvent('mouseover', { bubbles: true, cancelable: true }))
                                    // super hacky; trigger 'mouseover' twice
                                    window.requestAnimationFrame(() => {
                                        element.dispatchEvent(new MouseEvent('mouseover', { bubbles: true, cancelable: true }))
                                    })
                                    
                                })
                            }
                            const createMaterialIfNeeded = (defines) => {
                                if (defines.length == 0) {
                                    overrideShaderName = 'default'
                                    return
                                } 
                                defines.sort()
                                overrideShaderName = defines.join('_')
                                if (overrideShaderName in overrideMaterials) {
                                    return
                                }
                                const vertexSource = '//Custom shader\n' + defines.map(x => `#define ${x}`).join('\n') + '\n' + specialVertexShader
                                const fragmentSource = '//Custom shader\n' + defines.map(x => `#define ${x}`).join('\n') + '\n' + specialFragmentShader
                                const newVertex = context.createShader(context.VERTEX_SHADER)
                                const newFragment = context.createShader(context.FRAGMENT_SHADER)
                                context.oldShaderSource(newVertex, vertexSource)
                                context.compileShader(newVertex)
                                context.oldShaderSource(newFragment, fragmentSource)
                                context.compileShader(newFragment)
                                const newProgram = context.createProgram()
                                context.oldAttachShader(newProgram, newVertex)
                                context.oldAttachShader(newProgram, newFragment)
                                context.linkProgram(newProgram)
                                overrideMaterials[overrideShaderName] = newProgram
                                uniformLocations[overrideShaderName] = {}
                            }
                            window.addEventListener('message', (ev) => {
                                if (ev.data.type !== 'update-material') {
                                    return
                                }
                                createMaterialIfNeeded(ev.data.shaderMessage.defines || [])
                                customUniforms = ev.data.shaderMessage.uniforms
                                window.requestAnimationFrame(() => {
                                    draw()
                                })
                            })
                            const customShaderSource = (oldFunc) => function () {
                                const shader = arguments[0]
                                const source = arguments[1]
                                const retVal = oldFunc.apply(this, arguments)
                                if (source.includes('texture2DProj') && !source.startsWith('//Custom shader')) {
                                    shader.defaultShader = true
                                }
                                return retVal
                            }
                            const customAttachShader = (oldFunc) => function () {
                                const program = arguments[0]
                                const shader = arguments[1]
                                if (shader.defaultShader) {
                                    program.defaultProgram = true
                                }
                                return oldFunc.apply(this, arguments)
                            }
                            const customUseProgram = (oldFunc) => function () {
                                const oldProgram = arguments[0]
                                activeProgram = oldProgram
                                actualProgram = oldProgram
                                if (oldProgram.defaultProgram) {
                                    if (beforeDefines) {
                                        createMaterialIfNeeded(beforeDefines)
                                        beforeDefines = null
                                        customUniforms = beforeUniforms
                                        dirtyUniforms = true
                                    }
                                    const newProgram = overrideShaderName !== 'default' ? overrideMaterials[overrideShaderName] : oldProgram
                                    arguments[0] = newProgram
                                    actualProgram = newProgram
                                    if (dirtyUniforms) {
                                        dirtyUniforms = false
                                        oldFunc.apply(this, arguments)
                                        for (const uniformName in previousUniforms) {
                                            const data = previousUniforms[uniformName]
                                            const uniformFunc = data.func
                                            const args = data.args
                                            uniformLocations[overrideShaderName] ||= {}
                                            uniformLocations[overrideShaderName][uniformName] ||= context.oldGetUniformLocation(newProgram, uniformName)
                                            args[0] = uniformLocations[overrideShaderName][uniformName]
                                            uniformFunc.apply(this, args)
                                        }
                                        if (overrideShaderName !== 'default') {
                                            for (let idx = 0; idx < customUniforms.length; ++idx) {
                                                const uniform = customUniforms[idx]
                                                const uniformName= uniform.name
                                                uniformLocations[overrideShaderName][uniformName] ||= context.oldGetUniformLocation(newProgram, uniformName)
                                                const location = uniformLocations[overrideShaderName][uniformName]
                                                if (uniform.type == 'float') {
                                                    context.oldUniform1fv(location, uniform.value)
                                                } else if (uniform.type == 'vec2') {
                                                    context.oldUniform2fv(location, uniform.value)
                                                } else if (uniform.type == 'vec3') {
                                                    context.oldUniform3fv(location, uniform.value)
                                                }
                                            }
                                        }
                                        return
                                    }
                                }
                                actualProgram = arguments[0]
                                return oldFunc.apply(this, arguments)
                            }
                            const newGetUniformLocation = (oldFunc) => function () {
                                const program = arguments[0]
                                const name = arguments[1]
                                const ret = oldFunc.apply(this, arguments)
                                if (program.defaultProgram) {
                                    ret.uniformVariableName = name
                                    ret.program = program
                                 }
                                return ret
                            }
                            const newSetUniform = (oldFunc) => function () {
                                const program = activeProgram
                                const oldLocation = arguments[0]
                                if (program.defaultProgram) {
                                    previousUniforms[oldLocation.uniformVariableName] = {
                                        func: oldFunc,
                                        args: arguments
                                    }
                                    if (overrideShaderName !== 'default') {
                                        const materialProgram = overrideMaterials[overrideShaderName]
                                        if (materialProgram === actualProgram) {
                                            uniformLocations[overrideShaderName] ||= {}
                                            uniformLocations[overrideShaderName][oldLocation.uniformVariableName] ||= context.oldGetUniformLocation(materialProgram, oldLocation.uniformVariableName)
                                            const newLocation = uniformLocations[overrideShaderName][oldLocation.uniformVariableName]
                                            arguments[0] = newLocation
                                        } else {
                                            return
                                        }
                                    } else if (program !== actualProgram) {
                                        return
                                    }
                                }
                                return oldFunc.apply(this, arguments)
                            }

                            aliasConfig(context, {
                                attachShader: customAttachShader,
                                getUniformLocation: newGetUniformLocation,
                                shaderSource: customShaderSource,
                                uniform1f: newSetUniform,
                                uniform1fv: newSetUniform,
                                uniform1i: newSetUniform,
                                uniform1iv: newSetUniform,
                                uniform2f: newSetUniform,
                                uniform2fv: newSetUniform,
                                uniform2i: newSetUniform,
                                uniform2iv: newSetUniform,
                                uniform3f: newSetUniform,
                                uniform3fv: newSetUniform,
                                uniform3i: newSetUniform,
                                uniform3iv: newSetUniform,
                                uniform4f: newSetUniform,
                                uniform4fv: newSetUniform,
                                uniform4i: newSetUniform,
                                uniform4iv: newSetUniform,
                                uniformMatrix2fv: newSetUniform,
                                uniformMatrix3fv: newSetUniform,
                                uniformMatrix4fv: newSetUniform,
                                useProgram: customUseProgram,
                            })
                            window.removeEventListener('message', beforeCanvasListener)
                        }
                        return context
                    }
                })
            }
            return element
        }
    })
})()


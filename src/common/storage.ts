enum StorageKeys {
    BOT_NAME = 'bn',
    CHANNEL_NAME = 'cn',
    CG_WINDOW_OPEN = 'cwo',
    CG_READY = 'cgr',
    CG_GAME_ID = 'cggi',
    TWITCH_WINDOW_ID = 'twi',
    BLUR_BR_AVATARS = 'bba',
    BLUR_BR_GUESSES = 'bbg',
    HIDE_BR_NAMES = 'hbn',
    LOGS = 'logs',
    EVENTS = 'events',
    DEBUGGABLE = 'dbg',
    PIXELATE_MAP = 'pxl',
    PIXELATE_SCALE = 'pls',
    GRAYSCALE = 'gs',
    BLOCK_LIST = 'bl',
}

export default StorageKeys
import Game, { LatLong } from 'common/game'
import { GameScore, Guess, RoundScore } from 'common/scoring'
import AbyssLogger from './abyss_logger'
import { AbyssTag, ILogger } from './logging'

export enum MessageType {
    CONTENT_PING = 'content-ping',
    WORKER_PONG = 'worker-pong',
    FINISHED_FRAME = 'on-frame-finish',
    CONNECT_TWITCH = 'connect-twitch',
    DISCONNECT_TWITCH = 'disconnect-twitch',
    TWITCH_CONNECTION_SUCCESS = 'twitch-connection-success',
    TWITCH_CONNECTION_FAIL = 'twitch-connection-fail',
    CONNECT_CHANNEL = 'connect-channel',
    CHANNEL_CONNECTION_SUCCESS = 'channel-connection-success',
    CHANNEL_CONNECTION_FAIL = 'channel-connection-fail',
    BOT_NAME_READY = 'bot-name-ready',
    QUERY_POPUP_STATS = 'ask-popup-stats',
    ANSWER_POPUP_STATS = 'answer-popup-stats',
    UPDATE_ROUND_SCORES = 'update-round-scores',
    SEND_ROUND_SCORES = 'send-round-scores',
    SEND_GAME_SCORES = 'send-game-scores',
    CG_READY = 'cg-ready',
    CG_DISABLED = 'cg-disabled',
    CG_START_GAME = 'start-cg-game',
    CG_START_ROUND = 'start-round',
    CG_START_GUESSES = 'start-cg-guess',
    CG_STOP_GUESSES = 'stop-cg-guess',
    CG_ROUND_FINISHED = 'cg-round-finished',
    CG_GAME_FINISHED = 'cg-game-finished',
    TWITCH_WINDOW_CLOSED = 'twitch-close-window',
    BLUR_BR_AVATARS = 'blur-br-avatars',
    UNBLUR_BR_AVATARS = 'unblur-br-avatars',
    BLUR_BR_GUESSES = 'blur-br-guesses',
    UNBLUR_BR_GUESSES = 'unblur-br-guesses',
    HIDE_BR_NAMES = 'hide-br-names',
    UNHIDE_BR_NAMES = 'unhide-br-names',
    UPDATE_BR_DATA = 'update-br-data',
    LOG_INTO_ABYSS = 'log-into-abyss',
    DEBUG_ON = 'debug-on',
    DEBUG_OFF = 'debug-off',
    CLEAR_MAP_POINTS = 'clear-all',
    SHOW_MAP_POINTS = 'show-map-points',
    PIXELATE_ON = 'pixelate-on',
    PIXELATE_OFF = 'pixelate-off',
    GRAYSCALE_ON = 'grayscale-on',
    GRAYSCALE_OFF = 'grayscale-off',
    PIXELATE_SCALE = 'pixelate-scale',
    UPDATE_MATERIAL = 'update-material',
    UPDATE_BLOCKLIST = 'update-blocklist',
}

export interface MessageSender {
    sendMessage(msg: Message): void
}

export interface MessageListener {
    onMessage(msg: Message): void
    messageListenerName(): string
}

export class StreamerGuess {
    lat?: number
    lng?: number
    streamerName?: string
}

export interface MessageBroker {
    sendMessage(msg: Message): void
    registerListener(listener: MessageListener): void
    deregisterListener(listener: MessageListener): void
    deregisterAll(): void
}

export class ChromeMessageBroker implements MessageBroker {
    listeners: Map<MessageListener, any> = new Map()
    logger: ILogger
    constructor() {
        this.logger = new AbyssLogger(this).withTag(AbyssTag.BROKER)
    }
    sendMessage(msg: Message): void {
        chrome.runtime.sendMessage(msg)
    }
    registerListener(listener: MessageListener): void {
        const entry = (msg: Message) => {
            try {
                listener.onMessage(msg)
            } catch(e) {
                this.logger.log(`Failed to execute listener on ${listener.messageListenerName()} with message ${msg}: error is ${e}`)
            }
        }
        chrome.runtime.onMessage.addListener(entry)
        this.listeners.set(listener, entry)
    }
    deregisterListener(listener: MessageListener): void {
        const entry = this.listeners.get(listener)
        chrome.runtime.onMessage.removeListener(entry)
        this.listeners.delete(listener)
    }
    deregisterAll(): void {
        const listenersCopy = Array.from(this.listeners.keys())
        for (const listener of listenersCopy) {
            this.deregisterListener(listener)
        }
    }
}

export enum UniformType {
    FLOAT = 'float',
    VEC2 = 'vec2',
    VEC3 = 'vec3',
}
export class Uniform {
    name: string
    type: UniformType
    value?: number[]
}
export class ShaderMessage {
    defines?: string[]
    uniforms?: Uniform[]
}
export class BattleRoyaleRoundAnswer {
    countryCode?: string
}
export class BattleRoyaleRound {
    roundNumber: number
    answer?: BattleRoyaleRoundAnswer
    lat: number
    lng: number
}
export class BattleRoyaleData {
    gameId: string
    rounds: BattleRoyaleRound[]
}

/**
 * Messages between service worker and content script or popup
 */
export default class Message {
    type: MessageType
    game?: Game
    battleRoyaleData?: BattleRoyaleData
    channelName?: string
    abyssMessage?: string
    abyssTag?: AbyssTag
    failMessage?: string
    target?: LatLong
    roundGuesses?: Guess[]
    roundScores?: RoundScore[]
    gameScores?: GameScore[]
    streamerGuess?: StreamerGuess
    shaderMessage?: ShaderMessage
    scale?: number
    blockList?: string[]
}

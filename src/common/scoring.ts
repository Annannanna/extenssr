import { LatLong } from 'common/game'

export class RoundScore {
    username: string
    displayName: string
    dist: number
    score: number
    streak: number
}

export class Guess {
    username: string
    displayName?: string
    distance: number
    score: number
    position: LatLong
}

export function ScoreCompare(lhs: RoundScore, rhs: RoundScore): number {
    if (lhs.score == rhs.score) {
      if (lhs.dist == rhs.dist) {
        return lhs.username.localeCompare(rhs.username)
      }
      return lhs.dist - rhs.dist
    }
    return rhs.score - lhs.score
}

export class GameScore {
    username: string
    displayName: string
    totalDist: number
    totalScore: number
    playedRounds: number
    streak: number
}

export function GameScoreCompare(lhs: GameScore, rhs: GameScore): number {
    if (lhs.totalScore == rhs.totalScore) {
      if (lhs.totalDist == rhs.totalDist) {
        return lhs.username.localeCompare(rhs.username)
      }
      return lhs.totalDist - rhs.totalDist
    }
    return lhs.totalScore - rhs.totalScore
}

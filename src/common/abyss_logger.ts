import { ILogger, AbyssTag } from "common/logging"
import { MessageSender, MessageType } from "common/messages"

export default class AbyssLogger implements ILogger {
    messageSender: MessageSender
    tag: AbyssTag = AbyssTag.NONE
    constructor(messageSender: MessageSender) {
        this.messageSender = messageSender
    }
    withTag(tag: AbyssTag): ILogger {
        const newLogger = new AbyssLogger(this.messageSender)
        newLogger.tag = tag
        return newLogger
    }
    log(message: string): void {
        // if (this.tag == AbyssTag.NONE) {
        //     throw 'set a tag before sending this!'
        // }
        this.messageSender.sendMessage({
            type: MessageType.LOG_INTO_ABYSS,
            abyssTag: this.tag,
            abyssMessage: message
        })
    }
}
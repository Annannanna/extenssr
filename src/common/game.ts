/**
 * All these classes are part of the response to the /api/v3/game endpoint
 */


export class LatLong {
    lat?: number
    lng?: number
}

export class Bounds {
    min?: LatLong
    max?: LatLong
}

export enum GameMode {
    STANDARD = 'standard',
    STREAK = 'streak'
}

export enum GameType {
    STANDARD = 'standard'
}

export enum GameState {
    STARTED = 'started',
    FINISHED = 'finished'
}

export class GameRound {
    lat?: number
    lng?: number
    // TODO: Add missing fields
}

export class GameGuess {
    lat?: number
    lng?: number
    // TODO: Add rest
}

export class GamePlayer {
    guesses?: GameGuess[]
    // TODO: Add rest
}

export default class Game {
    token?: string
    bounds?: Bounds
    forbidMoving?: boolean
    forbidRotating?: boolean
    map?: string
    mapName?: string
    mode?: GameMode
    round?: number
    roundCount?: number
    rounds?: GameRound[]
    player?: GamePlayer
    state?: GameState
    type?: GameType
}
export enum AbyssTag {
    NONE = 'none',
    TWITCH_WINDOW = 'twitch_window.tsx',
    CHATGUESSR_GAME = 'chatguessr_game.ts',
    CHATGUESSR_GAME_PLUGIN = 'chatguessr_game_plugin.ts',
    CHATGUESSR_MAP_PLUGIN = 'chatguessr_map_plugin.ts',
    GAME_SCRIPT = 'game_script.ts',
    MAP_SCRIPT = 'map_script.ts',
    BATTLE_ROYALE_SCRIPT = 'battle_royale_script.ts',
    BATTLE_ROYALE_AVATARS_PLUGIN = 'battle_royale_avatars_plugin.ts',
    BATTLE_ROYALE_BLOCK_USERS_PLUGIN = 'battle_royale_block_users_plugin.ts',
    BATTLE_ROYALE_SHOW_LOCATIONS_PLUGIN = 'battle_royale_show_locations_plugin.ts',
    BATTLE_ROYALE_LOCATIONS = 'battle_royale_locations.tsx',
    PROFILE_SCRIPT = 'profile_script.ts',
    PROFILE_BLOCK_PLUGIN = 'profile_block_plugin.ts',
    ENDPOINT_TRANSITION_HANDLER = 'endpoint_transition_handler.ts',
    IRC = 'irc.ts',
    PUB_SUB = 'pubsub.ts',
    TWITCH_BOT = 'twitch_bot.ts',
    LEADERBOARD = 'leaderboard.tsx',
    WORKER = 'worker_main.ts',
    BROKER = 'messages.ts'
}

export interface ILogger {
    log(message: string): void
    withTag(tag: AbyssTag): ILogger
}

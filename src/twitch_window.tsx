import Message, { MessageType, MessageBroker, ChromeMessageBroker } from "common/messages"
import React, { useState, useEffect } from "react"
import ReactDOM from "react-dom"
import Button from "@material-ui/core/Button"
import Container from "@material-ui/core/Container"
import FormControl from "@material-ui/core/FormControl"
import Input from "@material-ui/core/Input"
import InputLabel from "@material-ui/core/InputLabel"
import CheckIcon from "@material-ui/icons/Check"
import HighlightOffIcon from "@material-ui/icons/HighlightOff"
import CloudCircle from "@material-ui/icons/CloudCircle"
import InputAdornment from "@material-ui/core/InputAdornment"
import Tooltip from "@material-ui/core/Tooltip"
import StorageKeys from "common/storage"
import TwitchBot from "twitch/twitch_bot"
import Bot, { BotConnectionListener, ConnectionStep } from "common/bot"
import ChatguessrGame from "chatguessr/chatguessr_game"
import { AbyssTag, ILogger } from "common/logging"
import AbyssLogger from "common/abyss_logger"

const App = (props: { bot: Bot; messageBroker: MessageBroker, logger: ILogger }): JSX.Element => {
  const [botName, setBotName] = useState("")
  const [channelName, setChannelName] = useState("")
  const [tempChannelName, setTempChannelName] = useState("")
  const [connectedToChannel, setConnectedToChannel] = useState(false)
  const [isConnecting, setIsConnecting] = useState(false)
  const appLogger = props.logger.withTag(AbyssTag.TWITCH_WINDOW)
  // storage listener
  useEffect(() => {
    const listener = (changes: { [key: string]: chrome.storage.StorageChange }) => {
      if (StorageKeys.BOT_NAME in changes) {
        setBotName(changes[StorageKeys.BOT_NAME].newValue)
      }
    }
    chrome.storage.onChanged.addListener(listener)
    return () => chrome.storage.onChanged.removeListener(listener)
  })

  useEffect(() => {
    appLogger.log("Connecting...")
    props.bot.connect(null)
  }, [])

  // connection listener
  useEffect(() => {
    appLogger.log("Connection listener...")
    const connectionListener: BotConnectionListener = {
      connectionFail: (step: ConnectionStep, error: string): void => {
        appLogger.log(`Fail at ${step}: ${error}`)
        setIsConnecting(false)
        switch (step) {
          case ConnectionStep.CHANNEL: {
            setConnectedToChannel(false)
            const msg: Message = {
              type: MessageType.CG_DISABLED,
            }
            props.messageBroker.sendMessage(msg)
            break
          }
        }
      },
      connectionSuccess: (step: ConnectionStep): void => {
        setIsConnecting(false)
        appLogger.log(`Successful ${step}`)
        switch (step) {
          case ConnectionStep.USERNAME_FETCH: {
            setBotName(props.bot.name())
            break
          }
          case ConnectionStep.PUBSUB: {
            // Once whispers work, we could try seeing if there's a saved channel name
            chrome.storage.local.get([StorageKeys.CHANNEL_NAME], (items) => {
              const name: string = items[StorageKeys.CHANNEL_NAME] ?? ""
              if (name != "") {
                setIsConnecting(true)
                setTempChannelName(name)
                props.bot.joinChannel(name)
              }
            })
            break
          }
          case ConnectionStep.CHANNEL: {
            const channelName = props.bot.channel()
            setTempChannelName(channelName)
            setChannelName(channelName)
            setConnectedToChannel(true)
            props.messageBroker.sendMessage({
              type: MessageType.CG_READY
            })
            props.messageBroker.sendMessage({
              type: MessageType.CHANNEL_CONNECTION_SUCCESS,
              channelName: channelName
            })
            break
          }
        }
      },
    }
    props.bot.setConnectionListener(connectionListener)
  }, [setConnectedToChannel, setChannelName, setIsConnecting])

  const channelAdornment = () => {
    if (isConnecting) {
        return <CloudCircle/>
    } else if (connectedToChannel && tempChannelName == channelName) {
      return <CheckIcon />
    }
    return <HighlightOffIcon />
  }

  return (
    <Container maxWidth="lg" style={{ maxHeight: "lg", padding: 0 }}>
      <FormControl disabled style={{ marginTop: 10, minWidth: "60%" }}>
        <InputLabel color="primary">Bot name:</InputLabel>
        <Input color="primary" value={botName} fullWidth></Input>
      </FormControl>
      <FormControl
        component="form"
        style={{ marginTop: 10, minWidth: "25ch" }}
        error={!connectedToChannel}
      >
        <InputLabel color="primary">Channel:</InputLabel>
        <Tooltip title="The name of your channel">
          <Input
            color="primary"
            value={tempChannelName}
            onChange={(ev) => setTempChannelName(ev.target.value.toLowerCase())}
            endAdornment={<InputAdornment position="end">{channelAdornment()}</InputAdornment>}
          />
        </Tooltip>
        <Button
          variant="contained"
          disabled={tempChannelName == "" || (tempChannelName == channelName && connectedToChannel) || isConnecting}
          type="submit"
          onClick={(e) => {
            props.bot.joinChannel(tempChannelName)
            setConnectedToChannel(false)
            setIsConnecting(true)
            e.preventDefault()
          }}
        >
          Connect
        </Button>
      </FormControl>
    </Container>
  )
}

const vars: { [id: string]: string } = Object.assign(
  {},
  ...window.location.href
    .split("?")[1]
    .split("&")
    .map((s) => {
      const ret = {}
      const qVal = s.split("=").map((x) => decodeURIComponent(x))
      ret[qVal[0]] = qVal[1]
      return ret
    })
)

const token: string = vars["token"]
const messageBroker: MessageBroker = new ChromeMessageBroker()
const logger: ILogger = new AbyssLogger(messageBroker)
const bot: Bot = new TwitchBot(token, logger)
const chatguessr_game: ChatguessrGame = new ChatguessrGame(bot, messageBroker, logger)

document.title = "Chatguessr admin panel"

window.addEventListener("beforeunload", () => {
  bot.disconnect()
  chatguessr_game.disconnected()
  messageBroker.deregisterAll()
})

ReactDOM.render(<App bot={bot} messageBroker={messageBroker} logger={logger} />, document.getElementById("container"))

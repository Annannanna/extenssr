import React, { useState, useEffect } from "react"
import ReactDOM from "react-dom"
import { Rnd } from "react-rnd"
import Container from "@material-ui/core/Container"
import AppBar from "@material-ui/core/AppBar"
import { makeStyles } from '@material-ui/core/styles';

import Box from "@material-ui/core/Box"
import { MessageBroker, MessageType } from "common/messages"
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import Switch from '@material-ui/core/Switch'
import { RoundScore, GameScore } from "common/scoring"
import { AbyssTag, ILogger } from "common/logging"

export interface ScoreListener {
  onNewGuesses(guesses: RoundScore[]): void
  onFinalScores(finalScores: GameScore[]): void
  onClearGuesses(): void
  onHideBoard(): void
  onShowBoard(): void
  onCanStopGuesses(value: boolean): void
}

export default class LeaderboardBroker {
  scoreListener?: ScoreListener = null
  setListener(scoreListener: ScoreListener): void {
    this.scoreListener = scoreListener
  }
  clearListener(): void {
    this.scoreListener = null
  }
  clearGuesses(): void {
    this.scoreListener?.onClearGuesses()
  }
  setGuesses(guesses: RoundScore[]): void {
    this.scoreListener?.onNewGuesses(guesses)
  }
  setFinalScores(scores: GameScore[]): void {
    this.scoreListener?.onFinalScores(scores)
  }
  hideBoard(): void {
    this.scoreListener?.onHideBoard()
  }
  showBoard(): void {
    this.scoreListener?.onShowBoard()
  }
  allowStopGuesses(value: boolean): void {
    this.scoreListener?.onCanStopGuesses(value)
  }
}

function Leaderboard(props: { leaderboardBroker: LeaderboardBroker, messageBroker: MessageBroker, logger: ILogger }): JSX.Element {
  const emptyScore: RoundScore[] = []
  const emptyFinalScore: GameScore[] = []
  const [canStopGuesses, setCanStopGuesses] = useState(false)
  const [scores, setScores] = useState(emptyScore)
  const [visible, setVisible] = useState(false)
  const [guessingEnabled, setGuessingEnabled] = useState(true)
  const [finalScores, setFinalScores] = useState(emptyFinalScore)
  const [gameEnded, setGameEnded] = useState(false)
  const useStyles = makeStyles({
    box: {
        color: 'lightgray',
        overflow: 'hidden',
        padding: 0,
        height: 30,
        textAlign: 'center'
    },
    td: {
      padding: 0
    },
    table: {
        backgroundColor: "#30303020",
    },
    draggableLeaderboard: {
        visibility: visible ? "visible" : "hidden",
        backgroundColor: "#30303090",
        overflow: "hidden",
        borderRadius: 15,
    }
  })
  const classes = useStyles()

  const entries = (sortedScores: RoundScore[]) => {
    return sortedScores.map((score: RoundScore, index: number) => {
      return (
        <TableRow key={`${JSON.stringify(score) + "_" + index}`}>
            <TableCell className={classes.td} key={`${JSON.stringify(score) + "_" + index + "_" + index}`}>
                <Box className={classes.box} textAlign={"center"} fontSize={20}> {index + 1}</Box>
            </TableCell>
            <TableCell className={classes.td} key={`${JSON.stringify(score) + "_" + index + "_" + score.displayName}`}>
                <Box className={classes.box} textAlign={"center"} fontSize={20}> {score.displayName}</Box>
            </TableCell>
            <TableCell className={classes.td} key={`${JSON.stringify(score) + "_" + index + "_" + score.displayName}`}>
                <Box className={classes.box} textAlign={"center"} fontSize={20}> {score.streak}</Box>
            </TableCell>
            <TableCell className={classes.td} key={`${JSON.stringify(score) + "_" + index + "_" + score.displayName}`}>
                <Box className={classes.box} textAlign={"center"} fontSize={20}> {`${score.dist.toFixed(1)} km`}</Box>
            </TableCell>
            <TableCell className={classes.td} key={`${JSON.stringify(score) + "_" + index + "_" + score.score}`}>
                <Box className={classes.box} textAlign={"center"} fontSize={20}> {score.score}</Box>
            </TableCell>
        </TableRow>
      )
    })
  }
  const finalEntries = (sortedFinalScores: GameScore[]) => {
    return sortedFinalScores.map((score: GameScore, index: number) => {
      return (
        <TableRow key={`${JSON.stringify(score) + "_" + index}`}>
            <TableCell key={`${JSON.stringify(score) + "_" + index + "_" + index}`}>
                <Box className={classes.box} textAlign={"center"} fontSize={20}> {index + 1}</Box>
            </TableCell>
            <TableCell key={`${JSON.stringify(score) + "_" + index + "_" + score.displayName}`}>
                <Box className={classes.box} textAlign={"center"} fontSize={20}> {score.displayName}</Box>
            </TableCell>
            <TableCell key={`${JSON.stringify(score) + "_" + index + "_" + score.displayName}`}>
                <Box className={classes.box} textAlign={"center"} fontSize={20}> {score.streak}</Box>
            </TableCell>
            <TableCell key={`${JSON.stringify(score) + "_" + index + "_" + score.displayName}`}>
                <Box className={classes.box} textAlign={"center"} fontSize={20}> {`${score.totalDist.toFixed(1)} km`}</Box>
            </TableCell>
            <TableCell key={`${JSON.stringify(score) + "_" + index + "_" + score.totalScore}`}>
                <Box className={classes.box} textAlign={"center"} fontSize={20}> {`[${score.playedRounds}]${score.totalScore}`}</Box>
            </TableCell>
        </TableRow>
      )
    })
  }
  useEffect(() => {
    props.leaderboardBroker.setListener({
      onNewGuesses: (guesses: RoundScore[]): void => {
        setScores(guesses)
        setGameEnded(false)
      },
      onFinalScores: (scores: GameScore[]): void => {
        setFinalScores(scores)
        setGameEnded(true)
      },
      onClearGuesses: (): void => {
        setScores(emptyScore)
      },
      onHideBoard: (): void => {
        setVisible(false)
      },
      onShowBoard: (): void => {
        setVisible(true)
        setGuessingEnabled(true)
      },
      onCanStopGuesses: (value: boolean): void => {
        setCanStopGuesses(value)
      }
    })
    return () => props.leaderboardBroker.clearListener()
  }, [setScores, setScores, setFinalScores, setGameEnded, setVisible, setCanStopGuesses, setGuessingEnabled])
  return (
    <Rnd
      default={{
        x: 0,
        y: 0,
        width: 500,
        height: 300,
      }}
      minWidth={500}
      className={classes.draggableLeaderboard}
    >
      <Container maxWidth={false} disableGutters style={{overflow: 'hidden'}}>
        <AppBar position='static' style={{ width: "100%", height: 30 }}>
            {canStopGuesses && <Switch color="default" checked={guessingEnabled} onChange={(evt) => {
                const allowed: boolean = evt.target.checked
                setGuessingEnabled(allowed)
                props.messageBroker.sendMessage({
                    type: allowed? MessageType.CG_START_GUESSES : MessageType.CG_STOP_GUESSES
                })
            }}/>
            }
        </AppBar>
        <TableContainer className={classes.table} component={Paper}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell className={classes.box} style={{width: "5%"}}>#</TableCell>
                        <TableCell className={classes.box} style={{width: "50%"}}>Player</TableCell>
                        <TableCell className={classes.box} style={{width: "5%"}}>Streak</TableCell>
                        <TableCell className={classes.box} style={{width: "20%"}}>Distance</TableCell>
                        <TableCell className={classes.box} style={{width: "20%"}}>Score</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                {gameEnded ? finalEntries(finalScores) : entries(scores)}
                </TableBody>
            </Table>
        </TableContainer>
      </Container>
    </Rnd>
  )
}

export function injectLeaderboard(leaderboardBroker: LeaderboardBroker, messageBroker: MessageBroker, logger: ILogger): void {
  if (document.getElementById("leaderboard")) {
    return
  }
  const localLogger = logger.withTag(AbyssTag.LEADERBOARD)
  localLogger.log('injectLeaderboard')
  const div = document.createElement("div")
  div.id = "leaderboard"
  document.querySelector(".layout__main").appendChild(div)
  ReactDOM.render(<Leaderboard leaderboardBroker={leaderboardBroker} messageBroker={messageBroker} logger={localLogger} />, div)
}

import Bot, { BotPrivateMessageListener, BotPublicMessageListener } from 'common/bot'
import Message, { MessageListener, MessageBroker, MessageType, StreamerGuess } from 'common/messages'
import Game, { LatLong } from 'common/game'
import Distance from '@turf/distance'
import { RoundScore, GameScore, ScoreCompare, Guess, GameScoreCompare } from 'common/scoring'
import { AbyssTag, ILogger } from 'common/logging'
import * as C from 'codegrid-js'
const GUESS_CMD = /!g (?<lat>-?[0-9]+(\.[0-9]*)?), (?<lng>-?[0-9]+(\.[0-9]*)?)/
enum State {
    INACTIVE,
    STARTED,
    READY,
    GUESSING,
    END_ROUND
}
class CooldownResponse {
    response: () => void
    cooldownLength: number
    private lastTimestamp = 0
    private getTime(): number {
        return Date.now()
    }
    constructor(response: { (): void; (): void }, cooldownLength: number) {
        this.response = response
        this.cooldownLength = cooldownLength
    }
    call(): void {
        if ((this.getTime() - this.lastTimestamp) > this.cooldownLength) {
            this.lastTimestamp = this.getTime()
            this.response()
        }
    }
}

export default class ChatguessrGame implements BotPrivateMessageListener, BotPublicMessageListener, MessageListener {
    private bot: Bot
    private messageBroker: MessageBroker
    private active = false
    private game?: Game = null
    private canGuess = false
    private maxDist: number
    private state: State = State.INACTIVE
    private roundGuesses: Map<string, LatLong> = new Map()
    private roundScores: Map<string, RoundScore> = new Map()
    private gameScores: Map<string, GameScore> = new Map()
    private logger: ILogger
    private codegrid: any
    private targetCode: string
    private perUserStreaks: Map<string, number> = new Map()
    private multiplier = 10
    private cgCommand = new CooldownResponse(() => { this.bot.sendPublicMessage(`To play, go to http://www.chatguessr.com/map/${this.bot.name()} click on where you think we are, and paste the command in chat (Ctrl+v / Command+v).`) },
        10000 /* =10s */)

    constructor(bot: Bot, messageBroker: MessageBroker, logger: ILogger) {
        this.bot = bot
        this.messageBroker = messageBroker
        this.messageBroker.registerListener(this)
        this.logger = logger.withTag(AbyssTag.CHATGUESSR_GAME)
        fetch(chrome.runtime.getURL('/tiles/worldgrid.json')).then((json) => {
            json.json().then((data) => {
                this.codegrid = C.CodeGrid(chrome.runtime.getURL('/tiles'), data)
            })
            
        })
    }

    messageListenerName(): string {
        return 'ChatguessrGame'
    }

    onMessage(message: Message): void {
        switch (message.type) {
            case MessageType.CG_READY: {
                this.ready()
                break
            }
            case MessageType.CG_START_GAME: {
                this.start(message.game)
                break
            }
            case MessageType.CG_START_ROUND: {
                this.newRound(message.game)
                break
            }
            case MessageType.CG_ROUND_FINISHED: {
                this.endRound(message.streamerGuess)
                break
            }
            case MessageType.CG_START_GUESSES: {
                this.unlockGuesses()
                break
            }
            case MessageType.CG_STOP_GUESSES: {
                this.lockGuesses()
                break
            }
            case MessageType.CG_GAME_FINISHED: {
                this.stop()
            }
        }
    }

    isActive(): boolean {
        return this.state != State.INACTIVE
    }

    ready(): void {
        this.state = State.READY
        this.bot.setPrivateMessageListener(this)
        this.bot.setPublicMessageListener(this)
        this.bot.sendPublicMessage(`${this.bot.name()} has entered the chat 🤖. Let's play some games!`)
    }

    newRound(game: Game): void {
        if (!this.isActive()) {
            return
        }
        if (this.state != State.GUESSING) {
            this.bot.sendPublicMessage(`Round ${game.round} has started! Start guessing!`)
        }
        this.state = State.GUESSING
        this.roundScores.clear()
        this.roundGuesses.clear()
        this.game = game
        this.multiplier = 10
        if (this.game && this.game.map == 'famous-places') {
            this.multiplier = 100
        }
        const target = this.game.rounds[this.game.round - 1]
        this.codegrid.getCode (target.lat, target.lng, (error, code) => {
            if (error) {
                this.targetCode = 'unknown'
            } else {
                this.targetCode = code
            }
        } )
        this.maxDist = Distance([this.game.bounds.min.lng, this.game.bounds.min.lat], [this.game.bounds.max.lng, this.game.bounds.max.lat])
        this.logger.log(`Max dist: ${this.maxDist}`)
        this.canGuess = true
    }

    private addStreamerGuess(streamerGuess: StreamerGuess): void {
        this.processGuess(streamerGuess.lat, streamerGuess.lng, this.bot.channel(), this.bot.channel(), true)
    }
    private computeBestGuesser(): string {
        const sortedScores = Array.from(this.roundScores.values()).sort(ScoreCompare)
        const bestGuess = sortedScores[0]
        return bestGuess.displayName ?? bestGuess.username
    }
    endRound(streamerGuess: StreamerGuess): void {
        if (!this.isActive() || this.state == State.END_ROUND) {
            return
        }
        this.state = State.END_ROUND
        this.logger.log('End round')
        this.addStreamerGuess(streamerGuess)
        this.roundScores.forEach((guess, username) => {
            if (this.gameScores.has(username)) {
                const score = this.gameScores.get(username)
                score.totalDist += guess.dist
                score.totalScore += guess.score
                score.playedRounds += 1
                score.streak = guess.streak
                this.gameScores.set(username, score)
            } else {
                this.gameScores.set(username, { username: username, displayName: guess.displayName, playedRounds: 1, totalDist: guess.dist, totalScore: guess.score, streak: guess.streak })
            }
        })
        const target = this.game.rounds[this.game.round - 1]
        const sortedScores = Array.from(this.roundScores.values()).sort(ScoreCompare)
        const roundGuesses: Guess[] = sortedScores.map((score) => {
            const guess: Guess = {
                username: score.username,
                displayName: score.displayName,
                position: this.roundGuesses.get(score.username),
                distance: score.dist,
                score: score.score
            }
            return guess
        })
        this.messageBroker.sendMessage({ type: MessageType.SEND_ROUND_SCORES, roundGuesses: roundGuesses, roundScores: sortedScores, target: { lat: target.lat, lng: target.lng } })
        this.bot.sendPublicMessage(`Round ${this.game.round} finished. ${this.computeBestGuesser()} won!`)
    }
    lockGuesses(): void {
        if (!this.isActive()) {
            return
        }
        if (!this.canGuess) {
            return
        }
        this.canGuess = false
        this.bot.sendPublicMessage(`Guesses are now closed.`)
    }
    unlockGuesses(): void {
        if (!this.isActive()) {
            return
        }
        if (this.canGuess) {
            return
        }
        this.canGuess = true
        this.bot.sendPublicMessage(`Guesses are open again.`)
    }
    start(game: Game): void {
        if (!this.game || this.game.token != game.token) {
            this.bot.sendPublicMessage(`A new game ${game.mapName ? (" of '" + game.mapName) + "'" : ''} has started. Good luck!`)
        }
        this.multiplier = 10
        if (this.game && this.game.map == 'famous-places') {
            this.multiplier = 100
        }
        this.state = State.STARTED
        this.game = game
        this.roundScores.clear()
        this.gameScores.clear()
    }
    stop(): void {
        if (!this.isActive()) {
            return
        }
        const sortedFinalScores = Array.from(this.gameScores.values()).sort(GameScoreCompare)
        this.messageBroker.sendMessage({ type: MessageType.SEND_GAME_SCORES, gameScores: sortedFinalScores })
        this.bot.sendPublicMessage(`Game is finished. Congrats ${sortedFinalScores[0].displayName ?? sortedFinalScores[0].username}`)
        this.roundScores.clear()
        this.gameScores.clear()
        this.state = State.INACTIVE
        this.game = null
    }
    disconnected(): void {
        this.bot.setPrivateMessageListener(null)
        this.bot.setPublicMessageListener(null)
    }
    private processGuess(lat: number, lng: number, user: string, displayName: string, streamerGuess = false): void {
        if (!this.canGuess && !streamerGuess) {
            return
        }
        
        this.roundGuesses.set(user, { lat: lat, lng: lng })
        const target = this.game.rounds[this.game.round - 1]
        const dist = Distance([target.lng, target.lat], [lng, lat])
        const score = Math.max(0, Math.ceil(5000 * Math.exp(-(this.multiplier * dist / this.maxDist))))
        this.codegrid.getCode (lat, lng, (error, code) => {
            let currentStreak = this.perUserStreaks.get(user) ?? 0
            if (code == this.targetCode) {
                currentStreak += 1
            } else {
                currentStreak = 0
            }
            this.perUserStreaks.set(user, currentStreak)
            const guess: RoundScore = { username: user, displayName: displayName, dist: dist, score: score, streak: currentStreak }
            this.roundScores.set(user, guess)
            const sortedScores = Array.from(this.roundScores.values()).sort(ScoreCompare)
            this.messageBroker.sendMessage({
                type: MessageType.UPDATE_ROUND_SCORES,
                roundScores: sortedScores
            })
        })
        
    }
    onPrivateMessage(message: string, sender: string, senderDisplayName?: string): void {
        if (!this.isActive()) {
            return
        }
        if (this.roundScores.has(sender)) {
            const user = senderDisplayName ?? sender
            this.bot.sendPublicMessage(`${user} already guessed`)
        }
        const guessMatch = GUESS_CMD.exec(message)
        if (guessMatch) {
            try {
                this.processGuess(parseFloat(guessMatch.groups.lat), parseFloat(guessMatch.groups.lng), sender, senderDisplayName ?? sender)
            } catch (e) {
                this.logger.log(`Error processing guess ${message} from ${senderDisplayName ?? sender}`)
            }

        }
    }
    onPublicMessage(message: string, sender: string, senderDisplayName?: string): void {
        if (message.match('!cg')) {
            this.cgCommand.call()
        }
    }
}
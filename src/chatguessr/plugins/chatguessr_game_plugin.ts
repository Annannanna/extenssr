import Game, { LatLong } from 'common/game'
import Message, { MessageListener, MessageType } from 'common/messages'
import { EndpointPlugin, injectScript } from 'content/content_script'
import GameScript, { GameScriptState } from 'content/game_script'
import StorageKeys from 'common/storage'
import LeaderboardBroker, { injectLeaderboard } from 'chatguessr/leaderboard'
import { Guess } from 'common/scoring'
import { AbyssTag } from 'common/logging'

/**
 * Plugin for the /game endpoint related to Chatguessr functionality
 */

export default class ChatguessrGamePlugin extends EndpointPlugin<GameScriptState, GameScript> implements MessageListener {
    roundId: number
    chatguessrGameId = ""
    lastState = 'none'
    leaderboardBroker: LeaderboardBroker

    constructor(endpoint: GameScript) {
        super(endpoint, AbyssTag.CHATGUESSR_GAME_PLUGIN)
        this.leaderboardBroker = new LeaderboardBroker()
        this.getMessageBroker().registerListener(this)

        new MutationObserver((mutations, observer) => {
            for (const mutation of mutations) {
                for (const newNode of mutation.addedNodes) {
                    const asScript = <HTMLScriptElement>(newNode)
                    if (asScript && asScript.src && asScript.src.startsWith('https://maps.googleapis.com/maps/api/js?')) {
                        asScript.onload = () => {
                            observer.disconnect()
                            injectScript('mapInject.js')
                        }
                    }
                }
            }
        }).observe(document.documentElement, { childList: true, subtree: true })
    }
    private inThisWindow(): boolean {
        return this.chatguessrGameId == this.endpointScript.gameId
    }

    onMessage(msg: Message): void {
        switch (msg.type) {
            case MessageType.CG_START_GAME: {
                this.chatguessrGameId = msg.game.token
                this.leaderboardBroker.allowStopGuesses(true)
                break
            }
            case MessageType.UPDATE_ROUND_SCORES: {
                if (this.inThisWindow()) {
                    this.leaderboardBroker.setGuesses(msg.roundScores ?? [])
                }
                return
            }
            case MessageType.SEND_ROUND_SCORES: {
                if (this.inThisWindow()) {
                    this.leaderboardBroker.showBoard()
                    this.leaderboardBroker.setGuesses(msg.roundScores ?? [])
                    this.plotMapPoints(msg.target, msg.roundGuesses)
                }
                break
            }
            case MessageType.SEND_GAME_SCORES: {
                this.leaderboardBroker.allowStopGuesses(false)
                this.chatguessrGameId = ""
                this.leaderboardBroker.showBoard()
                this.leaderboardBroker.setFinalScores(msg.gameScores ?? [])
                break
            }
            default: {
                return
            }
        }
        this.updateGameData()
    }

    messageListenerName(): string {
        return 'ChatguessrGamePlugin'
    }

    deinit(): void {
        this.leaderboardBroker.hideBoard()
        this.leaderboardBroker.clearGuesses()
        const msg: Message = {
            type: MessageType.CLEAR_MAP_POINTS
        }
        window.postMessage(msg, '*')
    }

    protected runImpl(): void {
        this.updateGameData()
    }

    private async plotMapPoints(target: LatLong, guesses: Guess[]): Promise<void> {
        const msg: Message = {
            type: MessageType.SHOW_MAP_POINTS,
            target: target,
            roundGuesses: guesses
        }
        window.postMessage(msg, '*')
    }

    private handleRoundResult(game: Game): void {
        if (!this.inThisWindow()) {
            return
        }
        this.leaderboardBroker.allowStopGuesses(false)
        const guess = game.player?.guesses[this.roundId - 1]
        const msg: Message = {
            type: MessageType.CG_ROUND_FINISHED,
            streamerGuess: {
                lat: guess.lat,
                lng: guess.lng
            }
        }
        this.getMessageBroker().sendMessage(msg)
    }

    async updateGameData(): Promise<void> {
        const state = this.getState()
        if (this.lastState == state) {
            return
        }
        this.lastState = state
        if (!this.isActive() || this.endpointScript.gameId != this.chatguessrGameId) {
            return
        }
        if (state == 'none') {
            return
        } else if (state == GameScriptState.FINAL_RESULT) {
            const msg: Message = {
                type: MessageType.CG_GAME_FINISHED
            }
            this.getMessageBroker().sendMessage(msg)
            return
        }
        try {
            const game: Game = await (await this.getClient().get<Game>(`/api/v3/games/${this.endpointScript.gameId}`)).data
            this.endpointScript.roundId = game.round
            if (state == GameScriptState.GUESS) {
                const msg: Message = {
                    type: MessageType.CG_START_ROUND,
                    game: game
                }
                this.getMessageBroker().sendMessage(msg)
                injectLeaderboard(this.leaderboardBroker, this.getMessageBroker(), this.endpointScript.getBaseLogger())
                this.leaderboardBroker.showBoard()
                this.leaderboardBroker.clearGuesses()
                this.leaderboardBroker.allowStopGuesses(true)
            } else if (state == GameScriptState.ROUND_RESULT) {
                this.roundId = game.player.guesses.length
                this.handleRoundResult(game)
            } else if (state == GameScriptState.FINAL_RESULT) {
                const msg: Message = {
                    type: MessageType.CG_GAME_FINISHED
                }
                this.getMessageBroker().sendMessage(msg)
            }
        } catch (err) {
            this.logger.log(err)
        }
    }

    init(): void {
        chrome.storage.local.get(StorageKeys.CG_GAME_ID, (items) => {
            this.chatguessrGameId = items[StorageKeys.CG_GAME_ID] ?? ""
        })
    }
}
import Game from 'common/game'
import { AbyssTag } from 'common/logging'
import Message, { MessageListener, MessageType } from 'common/messages'
import StorageKeys from 'common/storage'
import { EndpointPlugin } from 'content/content_script'
import MapScript, { MapState } from 'content/map_script'

/**
 * Plugin for the /maps endpoing related to Chatguessr functionality
 */
export default class ChatguessrMapPlugin extends EndpointPlugin<MapState, MapScript> implements MessageListener {
    canPlayChatguessr = false
    chatguessrNode?: HTMLElement = null
    chatguessrStartButton?: HTMLButtonElement = null
    constructor(mapScript: MapScript) {
        super(mapScript, AbyssTag.CHATGUESSR_MAP_PLUGIN)
        this.getMessageBroker().registerListener(this)
    }

    messageListenerName(): string {
        return 'ChatguessrMapPlugin'
    }

    onMessage(msg: Message): void {
        switch (msg.type) {
            case MessageType.CG_READY: {
                this.canPlayChatguessr = true
                break
            }
            case MessageType.CG_DISABLED: {
                this.canPlayChatguessr = false
                break
            }
            default: {
                return
            }
        }
        if (this.endpointScript.isActive()) {
            this.resetChatguessrButton()
        }
    }

    

    private removeChatguessrButton(force = false): void {
        if (!force) {
            if (this.canPlayChatguessr) {
                return
            }
            if (!this.chatguessrNode) {
                return
            }
        }
        
        if (this.chatguessrNode) {
            this.chatguessrNode.remove()
            this.chatguessrNode = null
            this.onChatguessrDeselect()
        }
    }

    init(): void {
        chrome.storage.local.get([StorageKeys.CG_READY, StorageKeys.CG_WINDOW_OPEN], (items) => {
            const ready = (items[StorageKeys.CG_READY] ?? 'false') == 'true'
            const windowOpen = (items[StorageKeys.CG_WINDOW_OPEN] ?? 'false') == 'true'
            this.canPlayChatguessr = ready && windowOpen
        })
    }

    private injectChatguessrButton(): void {
        if (!this.canPlayChatguessr) {
            return
        }
        if (this.chatguessrNode) {
            return
        }
        document.querySelectorAll('.game-settings__section > div.radio-boxes > div.radio-box').forEach((element) => {
            element.addEventListener('click', () => {
                this.endpointScript.select(element)
                this.onChatguessrDeselect()
            })
        })

        this.chatguessrNode = this.createChatguessrSelectionOption()
        this.endpointScript.singlePlayerButton.parentElement.insertBefore(this.chatguessrNode, this.endpointScript.singlePlayerButton)
        this.endpointScript.select(this.chatguessrNode)
        this.injectChatguessrStartButton()
    }

    private createChatguessrSelectionOption(): HTMLElement {
        const element = <HTMLElement>this.endpointScript.singlePlayerButton.cloneNode(true)
        element.onclick = () => {
            this.endpointScript.select(element)
            this.injectChatguessrStartButton()
        }
        element.removeAttribute('data-qa')
        element.querySelector('.radio-box__label').innerHTML = 'Chatguessr'
        const cgImg = element.querySelector('img')
        cgImg.alt = 'Chatguessr'
        cgImg.src = chrome.runtime.getURL('icons/chatguessr.png')
        return element
    }

    private resetChatguessrButton(): void {
        if (this.getState() == MapState.START_GAME) {
            this.injectChatguessrButton()
            this.removeChatguessrButton()
        }
    }

    private injectChatguessrStartButton(): void {
        const settingsCheckbox = this.endpointScript.getSettingsCheckbox()
        if (settingsCheckbox) {
            settingsCheckbox.style.display = 'none'
        }
        if (this.chatguessrStartButton) {
            return
        }
        const initialStartButton = this.endpointScript.getStartButton()
        this.chatguessrStartButton = <HTMLButtonElement>initialStartButton.cloneNode(true)
        this.chatguessrStartButton.querySelector('.button__label').innerHTML = "Start chatguessin'"
        this.chatguessrStartButton.removeAttribute('data-qa')
        const parent = initialStartButton.parentElement
        parent.appendChild(this.chatguessrStartButton)
        initialStartButton.style.display = 'none'
        this.chatguessrStartButton.onclick = async () => {
            const response: Game = await (await this.endpointScript.getClient().post<Game>('/api/v3/games', {
                map: this.endpointScript.mapId,
                type: 'standard',
                timeLimit: 0,
                forbidMoving: false,
                forbidZooming: false,
                forbidRotating: false
            })).data
            const newUrl = `https://www.geoguessr.com/game/${response.token}`
            this.getMessageBroker().sendMessage({
                type: MessageType.CG_START_GAME,
                game: response
            })
            window.location.assign(newUrl)
        }
    }

    private onChatguessrDeselect(): void {
        this.logger.log('chatguessr deselect')
        const settingsCheckbox = this.endpointScript.getSettingsCheckbox()
        if (this.chatguessrStartButton) {
            this.chatguessrStartButton.remove()
            this.chatguessrStartButton = null
        }
        const button = this.endpointScript.getStartButton()
        if (button) {
            button.style.display = 'inline-block'
        }
        if (settingsCheckbox) {
            settingsCheckbox.style.display = 'block'
        }

    }

    runImpl(): void {
        if (!this.canPlayChatguessr) {
            this.removeChatguessrButton(true)
            return
        }
        this.injectChatguessrButton()
    }

    deinit(): void {
        this.removeChatguessrButton(true)
    }

}
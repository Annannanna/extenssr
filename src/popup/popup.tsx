import React, { useState, useEffect } from "react"
import Container from "@material-ui/core/Container"
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab"
import AppBar from "@material-ui/core/AppBar"
import Box from "@material-ui/core/Box"
import PropTypes from "prop-types"
import Typography from "@material-ui/core/Typography"
import Checkbox from "@material-ui/core/Checkbox"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import FormGroup from "@material-ui/core/FormGroup"
import Button from "@material-ui/core/Button"
import { MessageBroker, MessageType } from "common/messages"
import StorageKeys from "common/storage"
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles"
import Switch from "@material-ui/core/Switch"
import Slider from "@material-ui/core/Slider"

function TabPanel(props): JSX.Element {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
}
export class PopupProps {
  messageBroker: MessageBroker
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& > *": {
        margin: theme.spacing(1),
      },
    },
  })
)

function Popup(props: PopupProps): JSX.Element {
  const [selectedTab, setSelectedTab] = useState(0)
  const [blurAvatars, setBlurAvatars] = useState(false)
  const [blurGuesses, setBlurGuesses] = useState(false)
  const [hideNames, setHideNames] = useState(false)
  const [debuggable, setDebuggable] = useState(false)
  const [pixelate, setPixelate] = useState(false)
  const [grayscale, setGrayscale] = useState(false)
  const [pixelateScaling, setPixelateScaling] = useState(75.0)
  const [cgEnabled, setCgEnabled] = useState(false)
  const classes = useStyles()
  useEffect(() => {
    chrome.storage.local.get(
      [StorageKeys.BLUR_BR_AVATARS, StorageKeys.BLUR_BR_GUESSES, StorageKeys.HIDE_BR_NAMES],
      (items) => {
        setBlurAvatars((items[StorageKeys.BLUR_BR_AVATARS] ?? "false") == "true")
        setBlurGuesses((items[StorageKeys.BLUR_BR_GUESSES] ?? "false") == "true")
        setHideNames((items[StorageKeys.HIDE_BR_NAMES] ?? "false") == "true")
      }
    )
  }, [])
  useEffect(() => {
    chrome.storage.local.get(
      [StorageKeys.DEBUGGABLE, StorageKeys.PIXELATE_MAP, StorageKeys.PIXELATE_SCALE, StorageKeys.GRAYSCALE, StorageKeys.CG_WINDOW_OPEN],
      (items) => {
        setDebuggable((items[StorageKeys.DEBUGGABLE] ?? "false") == "true")
        setPixelate((items[StorageKeys.PIXELATE_MAP] ?? "false") == "true")
        setGrayscale((items[StorageKeys.GRAYSCALE] ?? "false") == "true")
        setCgEnabled((items[StorageKeys.CG_WINDOW_OPEN] ?? "false") == "true")
        setPixelateScaling(parseFloat(items[StorageKeys.PIXELATE_SCALE] ?? "75.0"))
      }
    )
  }, [])
  return (
    <Container maxWidth="lg" style={{ maxHeight: "lg", padding: 0 }}>
      <AppBar position="static">
        <Tabs value={selectedTab} onChange={(_, value) => setSelectedTab(value)}>
          <Tab label="Chatguessr" />
          <Tab label="Battle Royale" />
          <Tab label="Map effects" />
          <Tab label="Debugging" />
        </Tabs>
      </AppBar>
      <TabPanel value={selectedTab} index={0}>
            <Button
              disabled={cgEnabled}
              variant="contained"
              color="primary"
              onClick={() => {
                props.messageBroker.sendMessage({type: MessageType.CONNECT_TWITCH})
                setCgEnabled(true)
              }}
            >
              Connect Chatguessr Bot
            </Button>
      </TabPanel>
      <TabPanel value={selectedTab} index={1}>
        <Container maxWidth="lg" style={{ maxHeight: "lg", padding: 0 }}>
          <FormGroup>
            <FormControlLabel
              control={
                <Checkbox
                  checked={blurAvatars}
                  onChange={(evt) => {
                    if (evt.target.checked) {
                      props.messageBroker.sendMessage({ type: MessageType.BLUR_BR_AVATARS })
                    } else {
                      props.messageBroker.sendMessage({ type: MessageType.UNBLUR_BR_AVATARS })
                    }
                    setBlurAvatars(evt.target.checked)
                  }}
                />
              }
              label="Blur avatars"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={blurGuesses}
                  onChange={(evt) => {
                    if (evt.target.checked) {
                      props.messageBroker.sendMessage({ type: MessageType.BLUR_BR_GUESSES })
                    } else {
                      props.messageBroker.sendMessage({ type: MessageType.UNBLUR_BR_GUESSES })
                    }
                    setBlurGuesses(evt.target.checked)
                  }}
                />
              }
              label="Blur guesses"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={hideNames}
                  onChange={(evt) => {
                    if (evt.target.checked) {
                      props.messageBroker.sendMessage({ type: MessageType.HIDE_BR_NAMES })
                    } else {
                      props.messageBroker.sendMessage({ type: MessageType.UNHIDE_BR_NAMES })
                    }
                    setHideNames(evt.target.checked)
                  }}
                />
              }
              label="Hide names"
            />
          </FormGroup>
        </Container>
      </TabPanel>
      <TabPanel value={selectedTab} index={2}>
        <FormControlLabel
          label="Pixelate"
          control={
            <Switch
              color="primary"
              checked={pixelate}
              onChange={(evt) => {
                const state = evt.target.checked
                props.messageBroker.sendMessage({
                  type: state ? MessageType.PIXELATE_ON : MessageType.PIXELATE_OFF,
                })
                setPixelate(state)
              }}
            />
          }
        />
        <Slider
          disabled={!pixelate}
          defaultValue={pixelateScaling}
          min={4.0}
          max={75.0}
          onChange={(event: any, newValue: number) => {
            setPixelateScaling(newValue)
            props.messageBroker.sendMessage({type: MessageType.PIXELATE_SCALE, scale: newValue})
          }}
        />
        <FormControlLabel
          label="Grayscale"
          control={
            <Switch
              color="primary"
              checked={grayscale}
              onChange={(evt) => {
                const state = evt.target.checked
                props.messageBroker.sendMessage({
                  type: state ? MessageType.GRAYSCALE_ON : MessageType.GRAYSCALE_OFF,
                })
                setGrayscale(state)
              }}
            />
          }
        />
      </TabPanel>
      <TabPanel value={selectedTab} index={3}>
        <FormControlLabel
          label="Enable debugging"
          control={
            <Switch
              color="primary"
              checked={debuggable}
              onChange={(evt) => {
                const state = evt.target.checked
                props.messageBroker.sendMessage({
                  type: state ? MessageType.DEBUG_ON : MessageType.DEBUG_OFF,
                })
                setDebuggable(state)
              }}
            />
          }
        />
        <div className={classes.root}>
          <div>
            <Button
              variant="contained"
              color="primary"
              disabled={!debuggable}
              onClick={() => {
                chrome.storage.local.get([StorageKeys.LOGS, StorageKeys.EVENTS], (items) => {
                  const a = document.createElement("a")
                  const file = new Blob([JSON.stringify(items)], { type: "application/json" })
                  a.href = URL.createObjectURL(file)
                  a.download = "bugreport.json"
                  a.click()
                  URL.revokeObjectURL(a.href)
                })
              }}
            >
              Download full bug report
            </Button>
          </div>
          <div>
            <Button
              disabled={!debuggable}
              variant="contained"
              color="primary"
              onClick={() => {
                chrome.storage.local.remove([StorageKeys.LOGS, StorageKeys.EVENTS])
              }}
            >
              Clear debugging data
            </Button>
          </div>
        </div>
      </TabPanel>
    </Container>
  )
}
export default Popup

import { AxiosInstance } from 'axios'
import { AbyssTag, ILogger } from 'common/logging'
import { MessageBroker } from 'common/messages'

export interface IEndpointScript {
    run(path: string): void
    deinit(newPath: string): void
    matches(path: string): boolean
}

interface IEndpointPlugin {
    init(): void
    run(): void
    deinit(newPath: string): void
}

export default abstract class EndpointScript<StateEnum> implements IEndpointScript {
    private httpClient: AxiosInstance
    private gameServer: AxiosInstance
    private active = false
    private state: StateEnum | 'none'
    private endPointPlugins: IEndpointPlugin[] = []
    private messageBroker: MessageBroker
    private baseLogger: ILogger
    readonly logger: ILogger
    constructor(httpClient: AxiosInstance, gameServer: AxiosInstance, messageBroker: MessageBroker, baseLogger: ILogger, abyssTag: AbyssTag) {
        this.httpClient = httpClient
        this.gameServer = gameServer
        this.messageBroker = messageBroker
        this.baseLogger = baseLogger
        this.logger = baseLogger.withTag(abyssTag)
        this.state = 'none'
    }
    addPlugin(plugin: IEndpointPlugin): void {
        this.endPointPlugins.push(plugin)
    }
    readonly getState = (): StateEnum | 'none' => {
        return this.state
    }
    readonly getBaseLogger = (): ILogger => {
        return this.baseLogger
    }
    protected abstract inferState(): StateEnum | 'none'
    readonly getClient = (): AxiosInstance => {
        return this.httpClient
    }
    readonly getGameServer = (): AxiosInstance => {
        return this.gameServer
    }
    readonly getMessageBroker = (): MessageBroker => {
        return this.messageBroker
    }
    readonly run = async (path: string): Promise<void> => {
        this.active = true
        this.init(path)
        this.endPointPlugins.forEach(plugin => plugin.init())
        this.state = this.inferState()
        this.endPointPlugins.forEach(plugin => {
            const rPlugin = async () => plugin.run()
            rPlugin()
        })
    }
    protected abstract init(path: string): void
    readonly isActive = (): boolean => {
        return this.active
    }
    public readonly deinit = (newPath: string): void => {
        this.active = false
        this.state = 'none'
        this.deinitImpl(newPath)
        this.endPointPlugins.forEach(plugin => {
            const rPluginDeinit = async () => plugin.deinit(newPath)
            rPluginDeinit()
        })
    }
    protected abstract deinitImpl(newPath: string): void
    abstract matches(path: string): boolean
}

export abstract class EndpointPlugin<StateEnum, T extends EndpointScript<StateEnum> = EndpointScript<StateEnum>> implements IEndpointPlugin {
    endpointScript: T
    readonly logger: ILogger
    constructor(endpointScript: T, abyssTag: AbyssTag) {
        this.endpointScript = endpointScript
        this.logger = endpointScript.getBaseLogger().withTag(abyssTag)
    }
    abstract init(): void
    readonly isActive = (): boolean => {
        return this.endpointScript.isActive()
    }
    readonly getState = (): StateEnum | 'none' => {
        return this.endpointScript.getState()
    }
    readonly run = async (): Promise<void> => {
        this.runImpl()
    }
    readonly getClient = (): AxiosInstance => {
        return this.endpointScript.getClient()
    }
    readonly getMessageBroker = (): MessageBroker => {
        return this.endpointScript.getMessageBroker()
    }
    abstract deinit(newPath: string): void
    protected abstract runImpl(): void
}

export function injectScript(filename: string, first = false): void {
    document.addEventListener('DOMContentLoaded', () => {
        const script = document.createElement('script')
        script.src = chrome.runtime.getURL(filename)
        if (!first) {
            document.body.appendChild(script)
        } else {
            const parent = document.head || document.body
            if (parent.firstChild) {
                parent.insertBefore(script, parent.firstChild)
            } else {
                parent.appendChild(script)
            }
        }
    })
}
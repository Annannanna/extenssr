import { AxiosInstance } from 'axios'
import { MessageBroker } from 'common/messages'
import {IEndpointScript} from 'content/content_script'
import GameScript from 'content/game_script'
import MapScript from 'content/map_script'
import BattleRoyaleScript from 'content/battle_royale_script'
import { AbyssTag, ILogger } from 'common/logging'
import ProfileScript from './profile_script'

/**
 * Handles transitions between pages.
 * Geoguessr is a single-page application, so the content script should mostly be loaded only once
 * per tab, except in the case of a manual refresh or back button
 * press.
 */
export default class EndpointTransitionHandler {
    private currPath = ""
    private currEndpointScript?: IEndpointScript = null
    private endpointScripts: IEndpointScript[]
    readonly logger: ILogger
    constructor(httpClient: AxiosInstance, gameServer: AxiosInstance, messageBroker: MessageBroker, logger: ILogger) {
        this.endpointScripts = [
            new GameScript(httpClient, gameServer, messageBroker, logger),
            new MapScript(httpClient, gameServer, messageBroker, logger),
            new BattleRoyaleScript(httpClient, gameServer, messageBroker, logger),
            new ProfileScript(httpClient, gameServer, messageBroker, logger),
        ]
        this.logger = logger.withTag(AbyssTag.ENDPOINT_TRANSITION_HANDLER)
        document.addEventListener('DOMContentLoaded', () => {
            const observer = new MutationObserver(() => {
                const path = window.location.href.substr('https://www.geoguessr.com'.length)
                if (this.currPath != path) {
                    this.onStateChanged(path)
                }
            })
            observer.observe(document.body, {
                childList: true,
                subtree: true
            })
        })
    }
    onStateChanged(path: string): void {
        this.logger.log(`Change in state to ${path}`)
        if (this.currEndpointScript) {
            this.currEndpointScript.deinit(path)
        }
        this.currPath = path
        for (const script of this.endpointScripts) {
            if (script.matches(path)) {
                this.currEndpointScript = script
                script.run(path)
                return
            }
        }
        this.currEndpointScript = null
    }
}
import EndpointScript from './content_script'
import { AxiosInstance } from 'axios'
import ChatguessrGamePlugin from 'chatguessr/plugins/chatguessr_game_plugin'
import { MessageBroker } from 'common/messages'
import { AbyssTag, ILogger } from 'common/logging'

export enum GameScriptState {
    GUESS = 'guess',
    ROUND_RESULT = 'round-result',
    FINAL_RESULT = 'final-result',
}

/**
 * Handles everything that happens under /game/<game_id>
 */


export default class GameScript extends EndpointScript<GameScriptState> {
    gameId: string
    guessMap?: HTMLElement = null
    resultsMap?: HTMLElement = null
    playMapAgainButton?: HTMLElement = null
    roundId = -1
    constructor(client: AxiosInstance, gameServer: AxiosInstance, messageBroker: MessageBroker, logger: ILogger) {
        super(client, gameServer, messageBroker, logger, AbyssTag.GAME_SCRIPT)
        this.addPlugin(new ChatguessrGamePlugin(this))
    }

    private clearStateProxies(): void {
        this.guessMap = null
        this.resultsMap = null
        this.playMapAgainButton = null
    }

    private getStateProxies(): void {
        this.guessMap = document.querySelector('.game-layout__guess-map')
        this.resultsMap = document.querySelector('.result-map__map')
        this.playMapAgainButton = document.querySelector("*[data-qa='play-same-map']")
    }

    protected inferState(): GameScriptState | 'none' {
        this.clearStateProxies()
        this.getStateProxies()

        if (this.guessMap?.childElementCount) {
            return GameScriptState.GUESS
        }
        if (this.resultsMap?.childElementCount) {
            if (this.playMapAgainButton) {
                return GameScriptState.FINAL_RESULT
            }
            return GameScriptState.ROUND_RESULT
        }
        return 'none'
    }

    matches(path: string): boolean {
        return path.startsWith('/game/')
    }

    protected init(path: string): void {
        this.gameId = path.split('/')[2]
    }

    protected deinitImpl(): void {
        this.clearStateProxies()
    }

}
import { AxiosInstance } from 'axios'
import EndpointScript from './content_script'
import { MessageBroker } from 'common/messages'
import { AbyssTag, ILogger } from 'common/logging'
import ProfileBlockPlugin from './plugins/profile/profile_block_plugin'


export default class ProfileScript extends EndpointScript<never> {
    profileId: string

    constructor(client: AxiosInstance, gameServer: AxiosInstance, messageBroker: MessageBroker, logger: ILogger) {
        super(client, gameServer, messageBroker, logger, AbyssTag.PROFILE_SCRIPT)
        this.addPlugin(new ProfileBlockPlugin(this))
    }

    protected init(path: string): void {
        this.logger.log('Profile script init')
        this.profileId = path.split('/')[2]
    }

    protected inferState(): 'none' {
        return 'none'
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    protected deinitImpl(_: string): void {
        // Nothing to do
    }

    matches(path: string): boolean {
        return path.startsWith('/user/')
    }
}
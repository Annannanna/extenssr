import { AxiosInstance } from 'axios'
import EndpointScript from './content_script'
import ChatguessrMapPlugin from '../chatguessr/plugins/chatguessr_map_plugin'
import { MessageBroker } from 'common/messages'
import { AbyssTag, ILogger } from 'common/logging'

export enum MapState {
    START_GAME = 'start-game',
    MAP_STATS = 'map-stats'
}

export default class MapScript extends EndpointScript<MapState> {
    mapId: string
    singlePlayerButton?: HTMLElement = null
    challengeButton?: HTMLElement = null
    settingsCheckbox?: HTMLElement = null

    constructor(client: AxiosInstance, gameServer: AxiosInstance, messageBroker: MessageBroker, logger: ILogger) {
        super(client, gameServer, messageBroker, logger, AbyssTag.MAP_SCRIPT)
        this.addPlugin(new ChatguessrMapPlugin(this))
    }

    protected init(path: string): void {
        this.logger.log('Map script init')
        this.mapId = path.split('/')[2]
        this.getStateProxies()
    }

    unselect(element?: HTMLElement): void {
        element?.classList.remove('radio-box--selected')
    }

    select(element: Element): void {
        document.querySelectorAll('.game-settings__section > div.radio-boxes > div.radio-box--selected').forEach((element) => {
            element.classList.remove('radio-box--selected')
        })
        element.classList.add('radio-box--selected')
        this.getSettingsCheckbox().style.display = 'block'
    }

    getStartButton(): HTMLButtonElement {
        return <HTMLButtonElement>document.querySelector('.game-settings__section>button')
    }

    getSettingsCheckbox(): HTMLElement {
        return <HTMLElement>document.querySelector('.game-settings__checkbox')
    }

    private getStateProxies(): void {
        this.singlePlayerButton = document.querySelector('*[data-qa="game-type-single-player"]')
        this.challengeButton = document.querySelector('*[data-qa="game-type-challenge"]')
    }

    protected inferState(): MapState {
        if (this.singlePlayerButton != null && this.challengeButton != null) {
            return MapState.START_GAME
        }
        return MapState.MAP_STATS
    }

    protected deinitImpl(newPath: string): void {
        // TODO
    }

    matches(path: string): boolean {
        return path.startsWith('/maps/')
    }
}
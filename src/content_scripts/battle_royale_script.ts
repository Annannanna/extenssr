import { AxiosInstance } from 'axios'
import { AbyssTag, ILogger } from 'common/logging'
import messages, { MessageBroker, MessageListener, MessageType, BattleRoyaleData } from 'common/messages'
import StorageKeys from 'common/storage'
import EndpointScript from './content_script'
import BattleRoyaleAvatarsPlugin from './plugins/battle_royale/battle_royale_avatars_plugin'
import BattleRoyaleBlockPlugin from './plugins/battle_royale/battle_royale_block_users_plugin'
import BattleRoyaleShowLocationsPlugin from './plugins/battle_royale/battle_royale_show_locations_plugin'

export enum BattleRoyaleState {
    IN_LOBBY = 'in-lobby',
    ONGOING = 'ongoing',
    ONGOING_LOST = 'ongoing-lost',
    FINISHED = 'finished',
}

export default class BattleRoyaleScript extends EndpointScript<BattleRoyaleState> implements MessageListener {
    gameId = ""
    finished = false

    constructor(client: AxiosInstance, gameServer: AxiosInstance, messageBroker: MessageBroker, logger: ILogger) {
        super(client, gameServer, messageBroker, logger, AbyssTag.BATTLE_ROYALE_SCRIPT)
        this.addPlugin(new BattleRoyaleAvatarsPlugin(this))
        this.addPlugin(new BattleRoyaleShowLocationsPlugin(this))
        this.addPlugin(new BattleRoyaleBlockPlugin(this))
        messageBroker.registerListener(this)
    }

    messageListenerName(): string {
        return 'BattleRoyaleEndpoint'
    }

    onMessage(_: messages): void {
        //
    }
    protected inferState(): BattleRoyaleState | 'none' {
        if (document.querySelector('.lobby')) {
            return BattleRoyaleState.IN_LOBBY
        }
        if (!this.finished) {
            if (document.querySelector('.popup-view[data-testid="knocked-out-modal"]')) {
                return BattleRoyaleState.ONGOING_LOST
            }
            return BattleRoyaleState.ONGOING
        }
        return BattleRoyaleState.FINISHED
    }

    extractGameId(path: string): string {
        const splits = path.split('/')
        return splits[splits.length - 1].split('?')[0]
    }

    protected init(path: string): void {
        this.gameId = this.extractGameId(path)
    }

    shouldSkipDeinit(newPath: string): boolean {
        return this.matches(newPath) && this.extractGameId(newPath) === this.gameId
    }

    protected deinitImpl(newPath: string): void {
        if (this.shouldSkipDeinit(newPath)) {
            return
        }
        this.finished = false
    }
    matches(path: string): boolean {
        return path.startsWith('/battle-royale/')
    }

}
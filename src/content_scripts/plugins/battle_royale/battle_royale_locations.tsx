import React, { useState, useEffect } from "react"
import ReactDOM from "react-dom"
import Message, {
  BattleRoyaleRound,
  MessageBroker,
  MessageListener,
  MessageType,
} from "common/messages"
import { AbyssTag, ILogger } from "common/logging"
import Button from "@material-ui/core/Button"

function Locations(props: {
  gameId: string
  messageBroker: MessageBroker
  logger: ILogger
}): JSX.Element {
  const [showLocations, setShowLocations] = useState(false)
  const emptyRound: BattleRoyaleRound[] = []
  const [roundLocations, setRoundLocations] = useState(emptyRound)
  useEffect(() => {
    const listener: MessageListener = {
      messageListenerName: () => "BattleRoyaleLocations",
      onMessage: (msg: Message) => {
        if (msg.type === MessageType.UPDATE_BR_DATA) {
          if (msg.battleRoyaleData.gameId === props.gameId) {
            setRoundLocations(msg.battleRoyaleData.rounds)
          }
        }
      },
    }
    props.messageBroker.registerListener(listener)
    return () => {
      props.messageBroker.deregisterListener(listener)
    }
  }, [])
  const roundElement = (roundData: BattleRoyaleRound) => {
    return (
      <div style={{ margin: 10, padding:10 }}>
        <div style={{backgroundColor: 'lightgray', borderRadius:2}}>
        <img
          width={26}
          height={26}
          src={`/static/flags/${roundData.answer.countryCode.toUpperCase()}.svg`}
          style={{ display: 'inline-block', margin:10, verticalAlign: 'middle' }}
        />
        <a
          style={{display:'inline-block', margin:10}}
          href={`http://maps.google.com/maps?q=&layer=c&cbll=${roundData.lat},${roundData.lng}`}
          target="blank"
        >
          Round {roundData.roundNumber}
        </a>
        </div>
      </div>
    )
  }

  return (
    <div>
      <div>
        <Button
          style={{ zIndex: 1 }}
          onClick={() => {
            setShowLocations(!showLocations)
          }}
          variant="contained"
          color="primary"
        >
          {(showLocations ? "Hide" : "Show") + " locations from finished rounds"}
        </Button>
      </div>
      <div style={{ zIndex: 1, position: "absolute", display: showLocations ? "block" : "none" }}>
        {roundLocations
          .filter((location) => location.answer && location.answer.countryCode)
          .map(roundElement)}
      </div>
    </div>
  )
}

let prevGameId = null
export function removeLocationsDiv(): void {
  const prevLocationElement = document.getElementById("brlocations")
  if (prevLocationElement) {
    prevLocationElement.remove()
  }
}
export function injectLocations(
  gameId: string,
  messageBroker: MessageBroker,
  logger: ILogger
): void {
  const prevLocationElement = document.getElementById("brlocations")
  if (prevLocationElement && gameId === prevGameId) {
    return
  }
  if (prevLocationElement) {
    prevLocationElement.remove()
  }
  prevGameId = gameId
  const localLogger = logger.withTag(AbyssTag.BATTLE_ROYALE_LOCATIONS)
  localLogger.log("injectLocations")
  const div = document.createElement("div")
  div.id = "brlocations"
  const parent = document.querySelector(".layout__main")
  parent.insertBefore(div, parent.firstChild)
  ReactDOM.render(
    <Locations gameId={gameId} messageBroker={messageBroker} logger={localLogger} />,
    div
  )
}

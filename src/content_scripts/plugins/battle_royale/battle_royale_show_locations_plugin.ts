import { AbyssTag } from "common/logging";
import { BattleRoyaleData, MessageType } from "common/messages";
import BattleRoyaleScript, { BattleRoyaleState } from "content/battle_royale_script";
import { EndpointPlugin } from "content/content_script";
import { injectLocations, removeLocationsDiv } from "./battle_royale_locations";

export default class BattleRoyaleShowLocationsPlugin extends EndpointPlugin<BattleRoyaleState, BattleRoyaleScript>  {
    listener = null

    constructor(endpointScript: BattleRoyaleScript) {
        super(endpointScript, AbyssTag.BATTLE_ROYALE_SHOW_LOCATIONS_PLUGIN)
    }

    private updateRoundData(roundsData: BattleRoyaleData): void {
        injectLocations(this.endpointScript.gameId, this.getMessageBroker(), this.logger)
        this.getMessageBroker().sendMessage({type: MessageType.UPDATE_BR_DATA, battleRoyaleData: roundsData})
    }

    init(): void {
        if (this.listener) {
            return
        }
        this.listener = (evt) => {
            const battleRoyaleData = evt.detail
            if (battleRoyaleData.gameId !== this.endpointScript.gameId) {
                return
            }
            this.updateRoundData({
                gameId: this.endpointScript.gameId,
                rounds: battleRoyaleData.rounds
            })
        }
        document.addEventListener('battle-royale-data', this.listener)
    }
    deinit(newPath: string): void {
        if (this.endpointScript.shouldSkipDeinit(newPath)) {
            return
        }
        if (this.listener) {
            document.removeEventListener('battle-royale-data', this.listener)
            this.listener = null
        }
        removeLocationsDiv()
    }
    protected runImpl(): void {
        if (this.getState() !== BattleRoyaleState.IN_LOBBY) {
            this.endpointScript.getGameServer().get(`/api/battle-royale/${this.endpointScript.gameId}`,{withCredentials: true}).then(response => {
                if (response.status !== 200) {
                    return
                }
                const data = response.data
                this.endpointScript.finished = data.status === 'Finished'
                this.updateRoundData({
                    gameId: data.gameId,
                    rounds: data.rounds
                })
            }).catch()
        }
    }
}
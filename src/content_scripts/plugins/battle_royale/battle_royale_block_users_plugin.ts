import { AbyssTag } from "common/logging";
import messages, { MessageListener, MessageType } from "common/messages";
import StorageKeys from "common/storage";
import BattleRoyaleScript, { BattleRoyaleState } from "content/battle_royale_script";
import { EndpointPlugin } from "content/content_script";

export default class BattleRoyaleBlockPlugin extends EndpointPlugin<BattleRoyaleState, BattleRoyaleScript> implements MessageListener {
    blockedProfiles: string[] = []
    styleNode: HTMLStyleElement = document.createElement('style')
    listener = null
    blockButtons = []
    observer = null
    constructor(endpoint: BattleRoyaleScript) {
        super(endpoint, AbyssTag.BATTLE_ROYALE_AVATARS_PLUGIN)
    }
    onMessage(msg: messages): void {
        if (msg.type === MessageType.UPDATE_BLOCKLIST) {
            this.blockedProfiles = msg.blockList
            this.updateStyle()
        }
    }
    private hideButtons() {
        this.blockButtons.forEach(el => {
            if (el.parentElement) {
                el.parentElement.removeChild(el)
            }
        })
        this.blockButtons = []
    }
    private updateStyle() {
        const innerHTML = this.blockedProfiles.map(profile => {
            return `
                a[href="/user/${profile}"] > * {
                    filter:blur(10px);
                }
                .blocked {
                    filter:blur(10px);
                }
            `
        }).join("\n") + `
            .lobby-block-unblock {
                width: 20px;
                height: 20px;
                position: absolute;
                left: 80%;
            }
            .blocked-user {
                filter:blur(10px);
            }
        `

        if (this.getState() === BattleRoyaleState.IN_LOBBY) {
            this.hideButtons()
            const players = document.querySelectorAll('.user-grid__item-backplate--visible > a')
            if (players) {
                players.forEach((player: HTMLAnchorElement) => {
                    if (player.href.endsWith('/me/profile')) {
                        return
                    }
                    const parts = player.href.split('/')
                    const playerId = parts[parts.length - 1]
                    const blocked = this.blockedProfiles.includes(playerId)
                    const button = document.createElement('img')
                    button.classList.add('lobby-block-unblock')
                    button.src = chrome.runtime.getURL(`icons/${blocked ? 'tick' : 'block'}.png`)
                    button.addEventListener('click', () => {
                        const newBlocklist = this.blockedProfiles.slice()
                        if (blocked) {
                            const idx = newBlocklist.indexOf(playerId)
                            if (idx !== -1) {
                                newBlocklist.splice(idx, 1)
                            }
                        } else {
                            newBlocklist.push(playerId)
                        }
                        this.getMessageBroker().sendMessage({ type: MessageType.UPDATE_BLOCKLIST, blockList: newBlocklist })
                    })
                    player.parentElement.insertBefore(button, player)
                    this.blockButtons.push(button)
                })
            }
        } 
        if (this.getState() !== 'none') {
            const players = document.querySelectorAll('.player-list__player-name > a')
            if (players) {
                players.forEach((player: HTMLAnchorElement) => {
                    if (player.href.endsWith('/me/profile')) {
                        return
                    }
                    const parts = player.href.split('/')
                    const playerId = parts[parts.length - 1]
                    const playerOverView = player.parentElement.parentElement
                    const playerAvatar = playerOverView.querySelector('.player-list__player-avatar')
                    playerAvatar.classList.remove('blocked')
                    if (this.blockedProfiles.includes(playerId)) {
                        playerAvatar.classList.add('blocked')
                    }
                })
            }
        }
        this.styleNode.innerHTML = innerHTML
    }
    messageListenerName(): string {
        return "BattleRoyaleBlockPlugin"
    }

    init(): void {
        chrome.storage.local.get([StorageKeys.BLOCK_LIST], (items) => {
            this.blockedProfiles = items[StorageKeys.BLOCK_LIST] ?? []
            this.updateStyle()
        })
        this.getMessageBroker().registerListener(this)
        document.head.appendChild(this.styleNode)
        this.observer = new MutationObserver((mutations) => {
            let mutatedPlayer = false
            mutations.forEach((mutation) => {
                const canMutate = (node: Node) => {
                    if (node) {
                        const a = node as HTMLAnchorElement
                        if (a && a.href && a.href.includes('/user/')) {
                            mutatedPlayer = true
                        }
                        if (node.childNodes) {
                            node.childNodes.forEach(canMutate)
                        }
                    }
                }
                mutation.addedNodes.forEach(canMutate)
                mutation.removedNodes.forEach(canMutate)
            })
            if (mutatedPlayer) {
                this.updateStyle()
            }
        })
        this.observer.observe(document.body, { childList: true, subtree: true })
    }
    deinit(): void {
        if (this.observer) {
            this.observer.disconnect()
            this.observer = null
        }
        this.getMessageBroker().deregisterListener(this)
        document.head.removeChild(this.styleNode)
        this.hideButtons()
        if (this.listener) {
            document.removeEventListener('battle-royale-update-players', this.listener)
            this.listener = null
        }
    }
    protected runImpl(): void {
        //
    }

}


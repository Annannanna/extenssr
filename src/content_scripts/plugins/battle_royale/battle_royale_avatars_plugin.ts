import { AbyssTag } from "common/logging";
import messages, { MessageListener, MessageType } from "common/messages";
import StorageKeys from "common/storage";
import BattleRoyaleScript, { BattleRoyaleState } from "content/battle_royale_script";
import { EndpointPlugin } from "content/content_script";

export default class BattleRoyaleAvatarsPlugin extends EndpointPlugin<BattleRoyaleState, BattleRoyaleScript> implements MessageListener{
    readonly blurAvatarStyleNode = document.createElement('style')
    readonly blurGuessesStyleNode = document.createElement('style')
    readonly hideNamesStyleNode = document.createElement('style')
    constructor(endpoint: BattleRoyaleScript) {
        super(endpoint, AbyssTag.BATTLE_ROYALE_AVATARS_PLUGIN)
        this.blurAvatarStyleNode.innerHTML = `
        .user-grid__circle img , .player-list__player-avatar img, .player-list__guess-avatar img {
           filter: blur(10px);
        }`

       this.blurGuessesStyleNode.innerHTML = `
       .game-state-overview__wrong-guesses img, .player-list__guess img, .life-icon img {
           filter: blur(10px);
       }`
       this.hideNamesStyleNode.innerHTML = `
       .user-nick {
           display: none;
       }`
    }
    onMessage(msg: messages): void {
        switch (msg.type) {
            case MessageType.BLUR_BR_AVATARS: {
                this.blurAvatars()
                break
            }
            case MessageType.UNBLUR_BR_AVATARS: {
                this.unblurAvatars()
                break
            }
            case MessageType.BLUR_BR_GUESSES: {
                this.blurGuesses()
                break
            }
            case MessageType.UNBLUR_BR_GUESSES: {
                this.unblurGuesses()
                break
            }
            case MessageType.HIDE_BR_NAMES: {
                this.hideNames()
                break
            }
            case MessageType.UNHIDE_BR_NAMES: {
                this.unhideNames()
                break
            }
        }
    }
    messageListenerName(): string {
        return "BattleRoyaleAvatarsPlugin"
    }
    blurAvatars(): void {
        this.logger.log('trying to blur')
        if (this.blurAvatarStyleNode.parentElement) {
            this.blurAvatarStyleNode.remove()
        }
        // Add this last
        document.head.appendChild(this.blurAvatarStyleNode)
    }
    unblurAvatars(): void {
        this.logger.log('trying to unblur')
        if (this.blurAvatarStyleNode.parentElement) {
            this.blurAvatarStyleNode.remove()
        }
    }
    blurGuesses(): void {
        this.logger.log('trying to blur')
        if (this.blurGuessesStyleNode.parentElement) {
            this.blurGuessesStyleNode.remove()
        }
        // Add this last
        document.head.appendChild(this.blurGuessesStyleNode)
    }
    unblurGuesses(): void {
        this.logger.log('trying to unblur')
        if (this.blurGuessesStyleNode.parentElement) {
            this.blurGuessesStyleNode.remove()
        }
    }
    hideNames(): void {
        this.logger.log('trying to blur')
        if (this.hideNamesStyleNode.parentElement) {
            this.hideNamesStyleNode.remove()
        }
        // Add this last
        document.head.appendChild(this.hideNamesStyleNode)
    }
    unhideNames(): void {
        this.logger.log('trying to unblur')
        if (this.hideNamesStyleNode.parentElement) {
            this.hideNamesStyleNode.remove()
        }
    }
    init(): void {
        chrome.storage.local.get([StorageKeys.BLUR_BR_AVATARS, StorageKeys.HIDE_BR_NAMES], (items) => {
            const shouldBlur = (items[StorageKeys.BLUR_BR_AVATARS] ?? 'false') == 'true'
            const shouldHideNames = (items[StorageKeys.HIDE_BR_NAMES] ?? 'false') == 'true'
            if (shouldBlur) {
                this.blurAvatars()
            } else {
                this.unblurAvatars()
            }
            if (shouldHideNames) {
                this.hideNames()
            } else {
                this.unhideNames()
            }
        })
        this.getMessageBroker().registerListener(this)
    }
    deinit(): void {
        this.getMessageBroker().deregisterListener(this)
    }
    protected runImpl(): void {
        //
    }

}

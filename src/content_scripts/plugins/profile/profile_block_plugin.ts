import { AbyssTag } from "common/logging";
import Messages, { MessageListener, MessageType } from "common/messages";
import StorageKeys from "common/storage";
import { EndpointPlugin } from "content/content_script";
import ProfileScript from "content/profile_script";

export default class ProfileBlockPlugin extends EndpointPlugin<never, ProfileScript> implements MessageListener {
    readonly blockLink = document.createElement('a')
    avatarDomElement?: HTMLDivElement
    blockedProfiles: string[] = []
    callback = null

    constructor(endpoint: ProfileScript) {
        super(endpoint, AbyssTag.PROFILE_BLOCK_PLUGIN)
    }
    onMessage(msg: Messages): void {
        if (msg.type == MessageType.UPDATE_BLOCKLIST) {
            this.blockedProfiles = msg.blockList
            this.updateBanButton()
        }
    }
    messageListenerName(): string {
        return "ProfileBlockPlugin"
    }

    init(): void {
        this.avatarDomElement = document.querySelector('.user-card__image')
        this.avatarDomElement.setAttribute("style", "text-align: center")
        chrome.storage.local.get([StorageKeys.BLOCK_LIST], (items) => {
            this.blockedProfiles = items[StorageKeys.BLOCK_LIST] ?? []
            this.updateBanButton()
        })
        this.getMessageBroker().registerListener(this)
        this.callback = () => {
            const currentlyBlocked = this.isBlocked()
            const newBlocklist = this.blockedProfiles.slice()
            const idx = newBlocklist.indexOf(this.endpointScript.profileId)
            if (idx !== -1) {
                newBlocklist.splice(idx, 1)
            }
            if (!currentlyBlocked) {
                newBlocklist.push(this.endpointScript.profileId)
            }
            this.blockedProfiles = newBlocklist
            this.getMessageBroker().sendMessage({
                type: MessageType.UPDATE_BLOCKLIST,
                blockList: newBlocklist
            })
        }
        this.blockLink.addEventListener('click', this.callback)
        this.avatarDomElement.appendChild(this.blockLink)
    }

    private isBlocked(): boolean {
        return this.blockedProfiles.includes(this.endpointScript.profileId)
    }

    private updateBanButton() {
        const blocked = this.isBlocked()
        if (blocked) {
            this.blockLink.innerHTML = 'Unblock'
        } else {
            this.blockLink.innerHTML = 'Block'
        }
    }

    deinit(): void {
        this.getMessageBroker().deregisterListener(this)
        if (this.callback) {
            this.blockLink.removeEventListener('click', this.callback)
            this.callback = null
        }
        this.avatarDomElement.removeChild(this.blockLink)
    }

    protected runImpl(): void {
        // Do nothing
    }

}

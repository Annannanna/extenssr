# Version 0.9.0
* Add feature to 'block' troll users in Battle Royale
    * It's a best effort approach, since there's currently no API for kicking users from a lobby
    * Trolling users can be blocked either from their [profile page](https://www.youtube.com/watch?v=k366G7THW3g) or from the [lobby](https://www.youtube.com/watch?v=lek89wT6-eo)
# Version 0.8.1
* Add icons
# Version 0.8.0
* Allow displaying visited locations in BR after rounds end
# Version 0.7.3
* Disable chatguessr button after being clicked
# Version 0.7.2
* Fix instability with changing materials on the fly
# Version 0.7.1
* Move connect to chatguessr button into popup
# Version 0.7.0
* Add support for custom filters:
    * Grayscale
    * Pixelated
# Version 0.6.9
* First 'official' version (every other commit has been for the same version)
* Chatguessr support:
    * !cg command
    * Guessing via whisper
    * Specific icon for choosing Chatguessr mode (alongside Single Player / Challenge)
    * Showing results on the map
    * Showing stats on a leaderboard
    * Proper scores (+/- 1 point)
    * Streaks via codegridjs
* Missing Chatguessr features:
    * Flags selection
    * Persistance of user scores/streaks
    * Ability to use external APIs for streaks
* BR features:
    * Hiding usernames
    * Blurring avatars
    * Blurring former guesses
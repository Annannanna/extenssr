const appName = 'Extenssr'
const background_script = 'worker.bundle.js'
const content_scripts = ['content.bundle.js']
const permissions = [
    // For logging in to Twitch
    'identity',
    // For persisting data locally
    'storage',
    'unlimitedStorage',
    // To catch when a webpage finishes loading
    'webNavigation',
]
const popup = 'popup.html'
const icons = {
    '48': 'icons/extenssr_48.png',
    '128': 'icons/extenssr_128.png',
}
const domains = ['https://www.geoguessr.com/*']
const extra_resources = ['icons/*.png',
    'mapInject.js',
    'canvasInject.js',
    'websocketInject.js',
    'tiles/*.json']

const commonManifest = (pkg) => {
    return {
        name: appName,
        version: pkg.version,
        description: pkg.description,
        icons: icons,
        content_scripts: [{
            matches: domains,
            run_at: 'document_start',
            js: content_scripts
        }],
        permissions: permissions
    }
}
const genV3 = (pkg) => {
    return Object.assign({}, commonManifest(pkg), {
        background: {
            service_worker: background_script
        },
        action: {
            default_popup: popup
        },
        web_accessible_resources: [
            {
                resources: extra_resources,
                matches: domains
            }
        ],
        manifest_version: 3
    })
}

const genV2 = (pkg) => {
    return Object.assign({}, commonManifest(pkg), {
        background: {
            scripts: [background_script]
        },
        browser_action: {
            default_popup: popup
        },
        web_accessible_resources: extra_resources,
        manifest_version: 2
    })
}

export default function generateManifest(manifestVersion: number, pkg: Object): Object {
    if (manifestVersion == 2) {
        return genV2(pkg)
    } else if(manifestVersion == 3) {
        return genV3(pkg)
    } else {
        throw 'Nooooo'
    }
}
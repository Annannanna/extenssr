# Extenssr

![Extenssr](./icons/extenssr.png)

A browser extension for extending the experience of [Geoguessr](https://www.geoguessr.com)

## Features

* [Chatguessr](https://chatguessr.com/) incomplete support
    * Still not stress tested
    * [Implemented features](https://www.youtube.com/watch?v=7GF_1S1JPrg)
        * Ability to connect to Twitch
        * Sending guesses via Twitch whispers
        * Correctish score calculation (off by 1 sometimes)
        * Correctish distance calculation (assumes spherical Earth)
        * Geocoding via [codegrid-js](https://github.com/hlaw/codegrid-js)
    * Missing features:
        * Flags
        * Leaderboards
        * Saving streaks between games
        * Ability to use a 3p API for geocoding (i.e. streak calculation)
* Battle Royale extensions
    * [Show visited locations at the end of round](https://www.youtube.com/watch?v=Qu9uZupIc90)
    * Blur player avatars
    * Hide player names
    * Blur incorrect guesses
* Map filter effects
    * Pixelate
    * Grayscale

# Reporting bugs

The project is still very new, so bugs are expected. If you encounter a reproducible bug, please do the following:

* (optional - see last bullet point) obtain a bugreport json
    * enable debugging (from the debugging panel of the extension pop-up menu)
    * reproduce the issue
    * download full bug report (same debugging panel)
    * there is no process to anonymise the contents or strip unnecessary personal data; the data is only used for the purpose of fixing bugs, ands is not shared with other parties. If that still makes you uncomfortable, you can skip this step.
* send the bug report (if you're ok with the caveats) alongside a description of the issue to `extenssr at gmail dot com`.

## Building

Building requires additional secrets and assets that are not included in the repository.
There is a sample `.env` file in `.env.template`. That contains the `CLIENT_ID` for connecting to the Twitch bot used by Chatguessr, a manifest key for the Chrome extension (required for keeping a stable appid when developing locally) and a `GUID` required if developing a Firefox add-on.

To build, simply run
```
    npm install
    npm run chrome #for a chrome extension
    npm run firefox #for firefox
```
And load the `extenssr_chrome` or `extenssr_firefox` directory as an unpacked extension.
The Chrome extension published to the Web store is built with `npm run chrome-nokey` (the key is added automatically by the submission process).

## TODO

* Make Chatguessr feature complete
    * Add flag support
* Improve Chatguessr?
    * Add graceful recovery support (i.e. recover from tab being refreshed or accidentally closed)
    * Add support for Discord?
* Improve Twitch window UI
* Add support for known scripts
    * Hide car and/or compass
* New difficulty boosters?
    * Disable 50/50 in public BR?
    * NM/NMP/NPMZ in BR?
    * Completely hide the map before countdown ends in BR?
* Country streaks in regular maps
* New game modes?
    * Endless mode (keep playing the same map for more than 5 rounds)
* Tests :(

# Acknowledgements

This is currently a project maintained by myself (`extenssr at gmail dot com`), but I stand on the shoulders of giants and rely on others to come up with good ideas.

Some of the folks below have donation links, just saying 😀.
* [@kyarosh_](https://www.instagram.com/kyarosh_) who designed the Extenssr icon.
* [Chatguessr](https://chatguessr.com/) folks, who conceived the original mod. This extension is not feature complete with their client, but could become an alternative for non-Windows streamers.
* [GeoGuessr Pro Community](https://discord.com/invite/xQQdRy5) which inspired a lot of the functionality. None of the scripts were copied, either due to licence incompatibility or because the extension uses different approaches.
    * ZackC__ who documented many of the Geoguessr API calls
    * [drparse](https://openuserjs.org/users/drparse) who created a bunch of map filter effects.
    * [SubSymmetry and theroei](https://www.reddit.com/r/geoguessr/comments/htgi42/country_streak_counter_script_automated_and/) who created the original country streak scripts.
    * Possibly others I forgot! Please send a pull request to add credit, or DM/email me.
* The Geoguessr community over on Twitch. Some folks who have helped either by suggesting features, or by alpha testing some of the functionality.
    * [El Porco](https://www.twitch.tv/el_porco_)
    * [Fran](https://twitch.tv/froonb)
    * [Jasmine](https://twitch.tv/jasminelune)
    * [Nhoa](https://twitch.tv/nhoska)
    * [ReAnna](https://twitch.tv/reanna__)
    * Probably many others I've forgotten, I'm sorry :(. Support your friendly local [Geoguessr streamers](https://www.twitch.tv/directory/game/GeoGuessr)!

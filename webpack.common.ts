import path from 'path'
import { Configuration, DefinePlugin } from 'webpack'
import CopyWebpackPlugin from 'copy-webpack-plugin'
import TsconfigPathsPlugin from "tsconfig-paths-webpack-plugin"
import generateManifest from './manifest_gen'

export default function genConfig(distDir: string, clientId: string, manifestVersion: number, key?: string, guid?: string): Configuration {
    // Hack because TsconfigPathsPlugin's apply function uses the wrong type for resolver
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const tsResolvePlugin: any = new TsconfigPathsPlugin({})
    const rootDir: string = path.resolve(__dirname, 'src')
    const contentPath = path.join(rootDir, 'content_main.ts')
    const workerPath = path.join(rootDir, 'worker_main.ts')
    const popupPath = path.join(rootDir, 'popup_main.tsx')
    const twitchPath = path.join(rootDir, 'twitch_window.tsx')
    const mapInject = path.join(rootDir, 'mapInject.js')
    const canvasInject = path.join(rootDir, 'canvasInject.js')
    const websocketInject = path.join(rootDir, 'websocketInject.js')
    return {
        mode: 'production',
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: 'ts-loader',
                    exclude: /node_modules/,
                }
            ],
        },
        resolve: {
            extensions: ['.ts', '.js', '.tsx'],
            plugins: [tsResolvePlugin]
        },
        entry: {
            "content": contentPath,
            "worker": workerPath,
            "popup": popupPath,
            "twitch_panel": twitchPath,
        },
        output: {
            path: distDir,
            filename: '[name].bundle.js'
        },
        plugins: [
            new DefinePlugin({
                __CLIENT_ID__: `'${clientId}'`
            }),
            new CopyWebpackPlugin({
                patterns: [
                    {
                        from: "package.json",
                        to: path.join(distDir, 'manifest.json'),
                        transform: (content) => {
                            const pkg = JSON.parse(content.toString())
                            const manifest = generateManifest(manifestVersion, pkg)
                            if (key) {
                                manifest['key'] = key
                            }
                            if (guid) {
                                manifest['browser_specific_settings'] = { 'gecko': { 'id': guid } }
                            }
                            return JSON.stringify(manifest)
                        }
                    },
                    {
                        from: "html",
                        to: distDir,
                    },
                    {
                        from: 'icons',
                        to: path.join(distDir, 'icons'),
                    },
                    {
                        from: 'node_modules/codegrid-js/tiles',
                        to: path.join(distDir, 'tiles'),
                    },
                    {
                        from: mapInject,
                        to: distDir
                    },
                    {
                        from: canvasInject,
                        to: distDir
                    },
                    {
                        from: websocketInject,
                        to: distDir
                    }
                ]
            })
        ]
    }
}
